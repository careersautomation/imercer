package pages.iMercer_pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaypalOrderPage {
	@FindBy(xpath = "//h1[contains(text(), 'Pay with PayPal')]")
	public static WebElement paypalPage_header;
	
	@FindBy(id = "email")
	public static WebElement email_input;
	
	@FindBy(id = "password")
	public static WebElement password_input;
	
	@FindBy(id = "btnLogin")
	public static WebElement login_button;
	
	@FindBy(id = "confirmButtonTop")
	public static WebElement payNow_button;
	
	public PaypalOrderPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void loginToPayPal() {
		System.out.println("size ----- "+driver.findElements(By.id("email")).size());
		if(driver.findElements(By.id("email")).size()>0)
		{
		setInput(email_input, "ashish_1291274154_biz@mercer.com");
		setInput(password_input, "123456789");
		clickElement(login_button);
		}
	}
	
	public void completePayment() {
		delay(3000);
		clickElement(payNow_button);
		delay(10000);
	}
}
