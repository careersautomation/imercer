package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import java.util.List;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentInformationPage {
	@FindBy(xpath = "//input[contains(@id, 'lineInvoiceInput')]")
	public static List<WebElement> invoiceMe_radiobutton;
	
	@FindBy(xpath = "//input[contains(@id, 'lineCCInput')]")
	public static List<WebElement> creditCart_radiobutton;
	
	@FindBy(id = "openorderdetail")
	public static WebElement continue_button;
	
	@FindBy(id = "adivshipping")
	public static WebElement deliverInformationTab_link;
	
	public PaymentInformationPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectPaymentMethod(List<WebElement> e) {
		for(WebElement ele: e)
		clickElement(ele);
	}

	
	public void continueToReviewOrderPage() {
		
		clickElement(continue_button);
	}
	
	public void navigateToDeliveryInfoStep() {
		clickElement(deliverInformationTab_link);
	}
}
