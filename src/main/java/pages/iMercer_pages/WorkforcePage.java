package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WorkforcePage {

	@FindBy(id = "ctl03_repBoxes_ctl00_lnkTitle")
	public static WebElement workforcePage_title;
	
	@FindBy(xpath = "//h1[contains(text(), 'Testing Product_Wizard_Car Policy')]")
	public static WebElement surveyTitle;
	
	public WorkforcePage() {
		PageFactory.initElements(driver, this);
	}
}
