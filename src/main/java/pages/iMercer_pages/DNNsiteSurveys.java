package pages.iMercer_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;
import static driverfactory.Driver.delay;

public class DNNsiteSurveys {
	@FindBy(id = "changeLocation")
	WebElement ChangeLocationDropdown;
	@FindBy(id = "lblSelectedRegion")
	public WebElement LocationLabel;
	@FindBy(id = "Header1_Repeater1_ctl01_lnkTabName")
	WebElement RewardsMenu;
	@FindBy(xpath = "//a[contains(text(),'Surveys & Repotrs')]")
	WebElement SurveysAndReportsOption;
	@FindBy(xpath = "//span[contains(text(),'SURVEYS AND REPORTS')]")
	public WebElement SurveysAndReportsPageHeader;
	@FindBy(id = "Span_compensation")
	WebElement ExpandCompensation;
	@FindBy(id = "Span_us-compensation")
	WebElement ExpandUS;
	@FindBy(id = "Span_department-function")
	WebElement ExpandDepartmentFunction;
	@FindBy(id = "Div_engineering-2")
	WebElement DepartmentFunction_Engineering;
	@FindBy(xpath = "//a[text()='Pre_Testing_HC']")
	WebElement PretestingHC;
	@FindBy(xpath = "//a[text() = 'June 6_Test_Hardcopy-This is for testing the list in Cart']")
	public static WebElement june6TestSurvey_link;
	@FindBy(xpath = "//a[text() = 'June7_Hard Copy and Online Access ABCD']")
	public static WebElement june7TestSurvey_link;
	@FindBy(css = ".SurveyTitle")
	public static WebElement SurveyTitle;

	@FindBy(id = "hcCCUserslnk")
	public static WebElement AddToCartButton;

	@FindBy(id = "opt8d097dd829834238b0ccdb01535a157a")
	public static WebElement selectQuantity_dropdown;

	@FindBy(id = "selectedQuantityPrice")
	public static WebElement quantitySelected_message;

	@FindBy(id = "totalPrice")
	public static WebElement totalPrice_text;

	@FindBy(id = "dnn_ctr563_ModuleContent")
	public static WebElement surveyAndReports_headerTitle;

	@FindBy(id = "dnn_ctr569_ModuleContent")
	public static WebElement surveyAndReportsCanada_headerTitle;
	
	@FindBy(xpath = "//a[contains(text(), 'dnn9_testing_ss')]")
	public static WebElement dnnSurvey_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Global_Testing_pdf')]")
	public static WebElement globalTestingPDFSurvey_link;
	
	@FindBy(xpath = "//a[text() = 'Canada_HCA']")
	public static WebElement canadaHCA_link;
	
	@FindBy(xpath = "//a[text() = 'Canada_CU']")
	public static WebElement canadaCU_link;
	
	@FindBy(xpath = "//a[text() = 'Denmark Oil Survey']")
	public static WebElement EuropeDenmarkOilSurvey_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Research & Insights')]")
	public static WebElement ResearchAndInsightsSurvey_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Research & Insights')]//preceding-sibling::i")
	public static WebElement researchAndInsight_arrowLink;

	@FindBy(xpath = "//input[@value = '6a480e8d624f4fc68f7ce226198ee9d5']/parent::label")
	public static WebElement participantYes_radiobutton1;
	
	@FindBy(xpath = "//input[@value = 'a2bc5c04503a40a488ad0c8b10d4fce0']/parent::label")
	public static WebElement participantYes_radiobutton;
	
	@FindBy(id = "ctl03_lnkProducts")
	WebElement AToZProductListingLink;
	
	@FindBy(id = "opt60143adfb09a49d59949d021bacc81ae")
	public static WebElement additionalUsers_dropdown;
	
	@FindBy(xpath = "//div[@id = 'hcPriceWrapper']//span")
	public static WebElement totalAmountLabel_concurrentUserSurvey;
	
	@FindBy(id = "optc3eb5ed3baa94e35b2f7c15159c39c0d")
	public static WebElement baseRegion_dropdown;
	
	@FindBy(id = "opt1c503f30-e5f6-452b-a1a5-e57c3b4d572a")
	public static WebElement baseCity_dropdown;
	
	@FindBy(id = "opt04a84a689fcb4555b953d6d0c9104374")
	public static WebElement hostRegion_dropdown;
	
	@FindBy(xpath = "//select[@id = 'listHostCityCollection']/option[@value = '400|e044b270-c63c-4978-9e57-7b10d9e55fa4']")
	public static WebElement hostCityOption;

	@FindBy(id = "btnAdd")
	public static WebElement add_button;
	
	@FindBy(id = "lbl_06f1db5e-36c9-4ee3-a581-f0bb12cbc933")
	public static WebElement survey1_checkbox;
	
	@FindBy(id = "lbl_7f9c2f35-a729-40e0-a416-fd289295e252")
	public static WebElement survey2_checkbox;
	
	
	@FindBy(id = "additionalselect")
	public static WebElement additionalUsers_SLMS_dropdown;
	
	@FindBy(id = "lbl_8e4ced65-1b79-44e8-a937-bc2dc4bfb8e0")
	public static WebElement survey1SLMS_checkbox;
	
	@FindBy(id = "lbl_b6b1938c-5c4e-46fc-9480-90e5117d7d7f")
	public static WebElement survey2SLMS_checkbox;
	

	@FindBy(id = "lbl_f0671266-1152-4ca7-bccb-384efa12c1a5")
	public static WebElement survey1MLMS_checkbox;
	
	@FindBy(id = "lbl_eb71c34b-c989-47c5-b3b7-8fa401bf90a5")
	public static WebElement survey2MLMS_checkbox;
	
	@FindBy(id = "lbl_f5353c11-ffb9-4437-8736-ea319109c2f5")
	public static WebElement survey1MLMS_checkbox1;
	
	@FindBy(id = "lbl_7f90e30b-1cd2-4cce-9c81-719b48ad8d4b")
	public static WebElement survey2MLMS_checkbox2;
	
	@FindBy(id = "lbl_cd4f48a1-e043-4aee-b1d4-6773048f5303")
	public static WebElement survey1Hardcopy_checkbox1;
	
	@FindBy(id = "lbl_718a2990-7f65-48a1-ba55-7c6e807efdfa")
	public static WebElement survey2Hardcopy_checkbox2;
	
	@FindBy(id = "optd13890ced31841fabe50bde4961cbf83")
	public static WebElement areYouParticipant_dropdown;
	
	@FindBy(id = "optd8e6fc427df44ad89b34731ecd55befb")
	public static WebElement selectParticipant_dropdown;
	
	@FindBy(id = "lblInvalidSelection")
	public static WebElement invalidSelection_errormessage;
	
	@FindBy(xpath = "//span[contains(text(), 'Narrow survey list by :')]")
	public static WebElement narrowSurveyList_text;
	
	@FindBy(id = "optef756e55fd314adf884592a9d5b94ec5")
	public static WebElement surveyReportAt100_dropdown;
	
	@FindBy(id = "opt1feb359fd77d462aa3b04d0eaa7a7fc8")
	public static WebElement surveyReportAt50_dropdown;
	
	
	@FindBy(id = "opt5a17ad22c1d94cd2aac2fbc8974d3e80")
	public static WebElement additionalUserCanada_dropdown;
	
	@FindBy(xpath = "//a[@href = '#BuyNow']")
	public static WebElement buyNow_button;
	
	@FindBy(id = "qty")
	public static WebElement denmarkOilQuantity_input;
	
	@FindBy(xpath = "//h2[text() = 'You may also be interested in:']")
	public static WebElement relatedSurvey_title;
	
	@FindBy(id = "Next")
	public static WebElement nextSurvey_button;
	
	@FindBy(id = "previous")
	public static WebElement previousSurvey_button;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/imercerdnn741/ca/talent-products/workplace-rewards/canada-compensation/engineering']")
	public static WebElement engineeringCanada_link;
	
	public DNNsiteSurveys() {
		PageFactory.initElements(driver, this);
	}

	public void changeLocation(String region) throws InterruptedException {
		hoverOverElement(driver, ChangeLocationDropdown);
		clickElement(driver.findElement(By.partialLinkText(region)));

	}

	public void openSurveysAndReportsPage() throws InterruptedException {
		hoverOverElement(driver, RewardsMenu);
		clickElement(SurveysAndReportsOption);
	}

	public void selectSurvey(String menu, String submenu1, String submenu2, String submenu3, String survey)
			throws InterruptedException {
		clickElement(driver.findElement(By.xpath("//a[text()='" + menu + "']//following::div[1]")));
		clickElement(driver.findElement(By.xpath(
				"//li[contains(@id,'" + menu.toLowerCase() + "')]//a[text()='" + submenu1 + "']//following::div[1]")));
		clickElement(driver.findElement(By.xpath(
				"//li[contains(@id,'" + menu.toLowerCase() + "')]//a[text()='" + submenu2 + "']//following::div[1]")));
		clickElementUsingJavaScript(driver, driver.findElement(
				By.xpath("//li[contains(@id,'" + menu.toLowerCase() + "')]//a[text()='" + submenu3 + "']")));
		clickElementUsingJavaScript(driver, driver.findElement(By.xpath("//a[text()='" + survey + "']")));
	}

	public void selectSurvey(String survey, String arg) throws InterruptedException {
		clickElementUsingJavaScript(driver, AToZProductListingLink);
		clickElement(driver.findElement(By.xpath("//a[text()='" + survey + "']")));
	}
	
	public void selectSurvey(WebElement e) {
		clickElement(e);
	}
	
	public void selectSurveyCategory(WebElement e) {
		clickElement(e);
	}

	public void selectSurveyQuantity(String value) {
		selEleByValue(selectQuantity_dropdown, value);
	}

	public void navigateToCartSummary() {
		// TODO Auto-generated method stub
		delay(2000);
		clickElement(AddToCartButton);

	}

	public void selectParticipant() {
		// TODO Auto-generated method stub
		clickElement(participantYes_radiobutton);
		delay(3000);
	}
	
	public void selectParticipant1() {
		// TODO Auto-generated method stub
		clickElement(participantYes_radiobutton1);
		delay(3000);
	}
	
	public void selectAdditionalUsers(String value) {
		selEleByValue(additionalUsers_dropdown, value);
	}
	
	public void selectHostAndBaseRegion(String baseRegion, String baseCity, String hostRegion) {
		selEleByValue(baseRegion_dropdown, baseRegion);
		selEleByValue(baseCity_dropdown, baseCity);
		selEleByValue(hostRegion_dropdown, hostRegion);
		clickElement(hostCityOption);
		clickElement(add_button);
	}

	public void selectMultiSurvey() {
		clickElement(survey1_checkbox);
		clickElement(survey2_checkbox);
	}

	public void selectAdditionalUserSLMS() {
		// TODO Auto-generated method stub
		selEleByValue(additionalUsers_SLMS_dropdown, "20.0000000000");
	}

	public void selectMultiSurveySLMS() {
		// TODO Auto-generated method stub
		clickElement(survey1SLMS_checkbox);
		clickElement(survey2SLMS_checkbox);
	}

	public void selectAreYouParticipant(String value) {
		selEleByValue(areYouParticipant_dropdown, value);
	}
	
	public void selectAreYouParticipant1(String value) {
		selEleByValue(selectParticipant_dropdown, value);
	}
	
	public void selectMultiSurveyMLMS() {
		// TODO Auto-generated method stub
		clickElement(survey1MLMS_checkbox);
		clickElement(survey2MLMS_checkbox);
	}

	public void selectMultiSurveyMLMS1() {
		// TODO Auto-generated method stub
		clickElement(survey1MLMS_checkbox1);
		clickElement(survey2MLMS_checkbox2);
	}
	
	public void selectMultiSurveyHardcopy() {
		// TODO Auto-generated method stub
		clickElement(survey1Hardcopy_checkbox1);
		clickElement(survey2Hardcopy_checkbox2);
	}
	
	public void selectAdditionalUserMLMS() {
		// TODO Auto-generated method stub
		selEleByValue(additionalUsers_SLMS_dropdown, "20.0000000000");
	}
	
	public void selectQuantityCanada(WebElement e, String value) {
		selEleByValue(e, value);
	}
	
	public void selectAdditionalUserCanadaCU() {
		selEleByValue(additionalUserCanada_dropdown, "2051099e1dce4643a53899289b66c83b");
	}
	
	public void buyNow() {
		clickElement(buyNow_button);
	}

	public void denmarkOilEnterQuantity(String qty) {
		setInput(denmarkOilQuantity_input, qty);
		
	}

	public void GoToNextSuggestion() {
		// TODO Auto-generated method stub
		clickElement(nextSurvey_button);
	}

	public void GoToPreviousSuggestion() {
		// TODO Auto-generated method stub
		clickElement(previousSurvey_button);
	}

	public void selectSurveyFromSuggestion() {
		// TODO Auto-generated method stub
		selectSurvey(june6TestSurvey_link);
	}
}
