
package pages.iMercer_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.iMercer_pages.MFA;

import static driverfactory.Driver.*;

public class OrderHistoryPage {

	
	@FindBy(xpath = "//h1[text()= 'Order History']")
	public static WebElement orderTitle;

	@FindBy(xpath = "//*[@id= 'dnn_ctr484_ModuleContent']/div/div[1]/table/tbody/tr[1]/td[2]/span")
	public static WebElement orderID_text;
	
	public OrderHistoryPage() {
		PageFactory.initElements(driver, this);
	}
}
