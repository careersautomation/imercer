package pages.iMercer_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.waitForElementToDisplay;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MFA {
	String Code;
	@FindBy(id = "ctl00_MainSectionContent_ButtonSubmit")
	WebElement SendCodeButton;
	@FindBy(id = "ButtonSubmit")
	public static WebElement submitCode_button;
	@FindBy(id = "ctl00_MainSectionContent_ChallengeCode")
	WebElement SubmitCode;
	@FindBy(id = "username")
	WebElement OutlookEmailID;
	@FindBy(id = "password")
	WebElement OutlookPassword;
	@FindBy(xpath = "//span[text()='MFA']")
	WebElement MFAtab;
	@FindBy(xpath="//div[@autoid='_lv_i']//following::div[1]")
	WebElement Message;
	@FindBy(xpath = "//div[@id='Item.MessageUniqueBody']//div[@class='PlainText']")
	WebElement MFAmessage;
	@FindBy(xpath = "//span[text()='Mark as read']")
	WebElement MarkAsRead;

	public MFA() {
		PageFactory.initElements(driver, this);
	}
	
	public void authenticate(String email, String password, String mfaChoice) throws InterruptedException {
		if(driver.getPageSource().contains("Verify your identity")){
		clickElement(driver.findElement(By.id("ctl00_MainSectionContent_Credentials_"+mfaChoice)));
		clickElementUsingJavaScript(driver,SendCodeButton);
		FetchMFACodeFromOutlookEmail(email, password);
		System.out.println("code is     "+ Code);
		setInput(SubmitCode,Code+Keys.SPACE+Keys.BACK_SPACE);
		clickElement(submitCode_button);
//		delay(6000);
//		waitForElementToEnable(SubmitCode);
	}}
	public void FetchMFACodeFromOutlookEmail(String email, String password) throws InterruptedException {
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get("https://apac1mail.mmc.com/OWA/");
		waitForElementToDisplay(OutlookEmailID);
		setInput(OutlookEmailID,email);
		setInput(OutlookPassword,password+Keys.ENTER);
		clickElement(MFAtab);
		clickElement(Message);
		Code = MFAmessage.getText();
		Code = Code.substring(27, 33);
		clickElement(MarkAsRead);
		switchToWindow("Welcome - Mercer");
	}
}

