package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LearningCenterPage {

	@FindBy(id = "ctl03_repBoxes_ctl00_lnkTitle")
	public static WebElement learningCenterPage_title;
	
	@FindBy(xpath = "//h1[contains(text(), 'A_June 10_COL QOL Testing')]")
	public static WebElement surveyTitle;
	
	
	public LearningCenterPage() {
		PageFactory.initElements(driver, this);
	}
}
