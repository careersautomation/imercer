package pages.iMercer_pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.verifyCheckBox;
import static driverfactory.Driver.selEleByValue;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.getElementText;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Factory;

public class BillingInformationPage {
	
	@FindBy(id = "shippingfirstname")
	public static WebElement firstName_input;
	
	@FindBy(id = "shippinglastname")
	public static WebElement lastName_input;
	
	@FindBy(id = "shippingcompany")
	public static WebElement companyName_input;
	
	@FindBy(id = "shippingaddress")
	public static WebElement StreetAddress_input;
	
	@FindBy(id = "shippingcountry")
	public static WebElement country_dropdown;
	
	@FindBy(id = "shippingstate")
	public static WebElement state_dropdown;
	
	@FindBy(id = "shippingcity")
	public static WebElement city_input;
	
	@FindBy(id = "shippingzip")
	public static WebElement zip_input;
	
	@FindBy(id = "shippingphone")
	public static WebElement contact_input;
	 
	@FindBy (id = "specialinstructions")
	public static WebElement CommentBox;
	
	@FindBy(id = "openpayment")
	public static WebElement continue_button;
	
	@FindBy(id = "chkTaxExempt")
	public static WebElement taxExempt_checkbox;
	

	@FindBy(id = "txtPaymentInfoCertificate")
	public static WebElement certificateNumber_input;
	
	@FindBy(xpath = "//strong[contains(text(), 'EXEMPT')]")
	public static WebElement taxExempt_label;
	
	@FindBy(xpath = "//strong[contains(text(), 'USD')]")
	public static WebElement taxValue_label;
	
	@FindBy(xpath = "//strong[contains(text(), 'USD 0.00')]")
	public static WebElement zeroTaxValue_label;
	
	@FindBy(xpath = "//a[contains(text(), 'Return policy')]")
	public static WebElement returnPolicy_link;
	
	@FindBy(id = "myModalLabel")
	public static WebElement returnPolicy_header;
	
	@FindBy(xpath = "//p[@class = 'returnPolicy']")
	public static WebElement returnPolicy_text;
	
	@FindBy(xpath = "//div[@class = 'modal-footer']/button[contains(text(), 'Close')]")
	public static WebElement returnPolicyClose_button;
	
	@FindBy(id = "txtshippingVAT")
	public static WebElement vat_input;
	
	public BillingInformationPage() {
		PageFactory.initElements(driver, this);
	}

	public void fillData(String state_value) {
		// TODO Auto-generated method stub
		delay(5000);
		setInput(firstName_input, "abc");
		setInput(lastName_input, "xyz");
		setInput(companyName_input, "Testing");
		setInput(StreetAddress_input, "1136 Avenue of America");
		selEleByValue(country_dropdown, "bf7389a2-9b21-4d33-b276-23c9c18ea0c0");
		delay(2000);
		selEleByValue(state_dropdown, state_value);
		delay(2000);
		setInput(city_input, "NewYork");
		delay(2000);
		setInput(zip_input, "10036");
		delay(2000);
		setInput(contact_input, "1234567890");
		
	}
	
	public void enterVATnumber(String vatNumber) {
	setInput(vat_input, vatNumber);	
	}
	
	
	public void selectTaxExempt() {
		int presentEle = driver.findElements(By.xpath("//td[@class = 'totaltax']/strong[contains(text(), 'EXEMPT')]")).size();
		System.out.println("is certificate number box present!!!       "+presentEle);
		if(presentEle == 0) {
			clickElement(taxExempt_checkbox);
			System.out.println("clicked");
		}
//		driver.findElement(By.id("txtPaymentInfoCertificate")).sendKeys("12345");
		setInput(certificateNumber_input, "12345");
		clickElement(CommentBox);
	}
	
	
	public void deSelectTaxExempt() {
		int presentEle = driver.findElements(By.xpath("//td[@class = 'totaltax']/strong[text() = 'EXEMPT']")).size();
		System.out.println("------------" +presentEle);
		if(presentEle > 0) {
			System.out.println("Clicking on deselect");
			clickElement(taxExempt_checkbox);
		}
		
	}
	
	public void continueToNextStep() {
		clickElement(continue_button);
	}

	public String getReturnPolicy() {
		// TODO Auto-generated method stub
		clickElement(returnPolicy_link);
		String returnPolicytext = getElementText(returnPolicy_text);
		clickElement(returnPolicyClose_button);
		return returnPolicytext;
	}
	
	
}
