package pages.iMercer_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementContains;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

public class OutlookMailPage {
	@FindBy(id = "username")
	WebElement OutlookEmailID;
	@FindBy(id = "password")
	WebElement OutlookPassword;
	@FindBy(xpath = "//span[text()='iMercer']")
	WebElement iMercertab;
	@FindBy(xpath="(//div[@autoid='_lv_i']//following::div[contains(@id, 'ariaId')])[1]")
	WebElement Message;
	@FindBy(xpath = "//span[@class = 'spnEmailText']")
	WebElement purchaseOrdermessage;
	@FindBy(xpath = "//span[text()='Mark as read']")
	WebElement MarkAsRead;
	@FindBy(xpath = "//td[@class = 'ValueSection']")
	public static WebElement orderValue_text;
	
	public OutlookMailPage() {
		PageFactory.initElements(driver, this);
	}
	
	public String FetchorderIDFromOutlookEmail(String email, String password, String currency, ExtentTest test) throws InterruptedException {

		switchToWindow("Outlook Web App");
		clickElement(iMercertab);
		clickElement(Message);
		String emailText = purchaseOrdermessage.getText();
		emailText = (emailText.split("#"))[1];
		String orderID = emailText.toString().split(". ")[0];
		System.out.println(orderID);
		verifyElementContains(orderValue_text, currency, "Email content are as per the expected curreny",test);
		clickElement(MarkAsRead);
		switchToWindow("iMercer > Checkout");
		return orderID;
	}
	

}
