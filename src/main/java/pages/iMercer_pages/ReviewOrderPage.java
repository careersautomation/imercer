package pages.iMercer_pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReviewOrderPage {
	@FindBy(id = "hcConfirmButton")
	public static WebElement submitOrder_button;
	
	public ReviewOrderPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void submitOrder() {
		clickElement(submitOrder_button);
	}
}
