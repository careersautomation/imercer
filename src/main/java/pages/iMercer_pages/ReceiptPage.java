package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReceiptPage {
	@FindBy(xpath = "//p[contains(text(), 'THANKS FOR YOUR ORDER!')]")
	public static WebElement receipt_title;
	
	@FindBy(xpath = "(//span[@class = 'ReceiptSubHeadings'])[2]")
	public static WebElement orderID_text;
	
	public ReceiptPage() {
		PageFactory.initElements(driver, this);
	}
}
