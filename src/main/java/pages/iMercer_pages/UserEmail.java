package pages.iMercer_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static ui.smoke.testcases.iMercer.iMercer_Smoke.MFAEmailId;
import static ui.smoke.testcases.iMercer.iMercer_Smoke.MFAEmailPassword;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserEmail {

	@FindBy(id = "username")
	WebElement OutlookEmailID;
	@FindBy(id = "password")
	WebElement OutlookPassword;
	@FindBy(xpath = "//span[@title='iMercer']")
	WebElement iMercertab;
	@FindBy(xpath = "//span[text()='Unread']")
	WebElement UnreadMenu;
	@FindBy(xpath="//div[@autoid='_lv_i']//following::div[1]")
	WebElement Message; 
	@FindBy(xpath = "//span/span[text()='HRSolutions.AP']")
	public WebElement MessageSenderLegacy;
	@FindBy(xpath = "//span//span[text()='QA_SurveysTest@mercer.com']")
	public WebElement MessageSenderDNN; 
	@FindBy(xpath = "//span[text()='Mark as read']")
	WebElement MarkAsRead;
	
	public UserEmail() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void openOutlookEmail() throws InterruptedException {
		if(driver.getWindowHandles().size()==1) {
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get("https://apac1mail.mmc.com/OWA/");
		waitForElementToDisplay(OutlookEmailID);
		setInput(OutlookEmailID,MFAEmailId);
		setInput(OutlookPassword,MFAEmailPassword+Keys.ENTER);
		}
		else
		{
			switchToWindow("Outlook");
		}
		waitForElementToDisplay(iMercertab);
		clickElement(iMercertab);
		clickElement(UnreadMenu);
		clickElement(Message);
		//Subject=driver.findElement(By.xpath("//span[text()='Order#"+OrderId_Legacy+" Product Request' and @role='heading']"));
		}
	
	
	
	public void switchToMainWindow()
	{
		clickElement(MarkAsRead);
//		((JavascriptExecutor)driver).executeScript("window.close()");
		switchToWindow("iMercer");
	}
}
