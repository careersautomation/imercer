package pages.iMercer_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.setInput;
import static utilities.SikuliActions.hover;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderMenuPage {
	@FindBy (id= "Header1_Repeater1_ctl01_lnkTabName")
	public static WebElement rewards_menu;
	
	@FindBy(id = "Header1_Repeater1_ctl02_lnkTabName")
	public static WebElement industries_menu;;
	
	@FindBy(id = "Header1_Repeater1_ctl03_lnkTabName")
	public static WebElement mobility_menu;
	
	@FindBy(id = "Header1_Repeater1_ctl04_lnkTabName")
	public static WebElement workforce_menu;
	
	@FindBy(id = "Header1_Repeater1_ctl05_lnkTabName")
	public static WebElement learningCenter_menu;
	
	@FindBy(id = "Header1_Repeater1_ctl06_lnkTabName")
	public static WebElement services_menu;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/imercerdnn741/Industries-and-consumer-goods1']")
	public static WebElement rewards_menuUnitedStates;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/iMercerDNN741/products/June-4_Test_PDF?isPartOpen=true']")
	public static WebElement industries_menuUnitedStates;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/iMercerDNN741/products/June-4_Test_OnlineAccess']")
	public static WebElement mobility_menuUnitedStates;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/iMercerDNN741/products/A1_Sample-Product_Wizard_Car-Policy']")
	public static WebElement workforce_menuUnitedStates;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/iMercerDNN741/products/June-10_COLQOL-Testing?isPartOpen=true']")
	public static WebElement learningCenter_menuUnitedStates;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/imercerdnn741/Products/Test-Concurrents-User_Apr-27AB']")
	public static WebElement services_menuUnitedStates;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/CA/tabs/products.aspx']")
	public static WebElement rewards_menuCanada;
	
	@FindBy(xpath = "//a[contains(text(), 'Surveys & Reports')]")
	public static WebElement surveyAndReports_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Compensation And Remuneration')]")
	public static WebElement compensationRemuneration_link;
	
	@FindBy(xpath = "//a[contains(text(), '123')]")
	public static WebElement bullet123_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Industry Solutions category')]")
	public static WebElement industrySolutionCategory_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Consumer Goods')]")
	public static WebElement consumerGood_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Core Solutions')]")
	public static WebElement coreSolutions_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Quality of Living')]")
	public static WebElement qualityOfLiving_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Testing1')]")
	public static WebElement testing1_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Communications')]")
	public static WebElement Communications_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Health Care Communication Solutions')]")
	public static WebElement healthCareComminication_link;

	@FindBy(xpath = "//a[contains(text(), 'Global Diversity Forum')]")
	public static WebElement globalDiversityForum_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Test_category 2')]")
	public static WebElement testCategory2_link;
	
	@FindBy(xpath = "//a[contains(text(), 'US Events/Trainings On-Demand')]")
	public static WebElement usEvent_link;
	
	@FindBy(xpath = "//a[contains(text(), 'IPE eLearning')]")
	public static WebElement IPElearning_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Consulting')]")
	public static WebElement consulting_link;
	
	@FindBy(xpath = "//a[@href = 'https://qa12-www2.imercer.com/imercerdnn741/Services/Services']")
	public static WebElement services_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Testing for Non reg users')]")
	public static WebElement testingForNonVegUser_link;
	
	@FindBy(id = "btnCookieOptIn")
	public static WebElement cookie_button;
	
	@FindBy(id = "cookies-consent")
	public static WebElement cookie_footer;
	
	@FindBy(xpath = "//div[@id = 'cart']/a//p")
	public static WebElement cartFlyout_menu;
	
	@FindBy(xpath = "//a[@class = 'cart_lnkEditItem'][contains(text(), 'Edit Item')]")
	public static WebElement editCart_flyoutMenu;
	
	@FindBy(id = "ctl03_lnkProducts")
	public static WebElement a_zProductListing_link;

	@FindBy(xpath = "//a[contains(text(), 'Testing789')]")
	public static WebElement testing789_link;
	
	@FindBy(xpath = "//a[contains(text(), 'Testing5')]")
	public static WebElement testing5_link;
	
	
	@FindBy(id = "Header1_txtSearch")
	public static WebElement search_input;
	
	@FindBy(id = "Header1_lnkBtnSearch")
	public static WebElement search_button;
	
	public HeaderMenuPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void naviagteViaMenu(WebElement menu, WebElement subMenu) {
		hoverOverElement(driver, menu);
		hoverAndClickOnElement(driver, menu, subMenu);

	}
	
	public void acceptcookie() {

			clickElement(cookie_button);
			System.out.println("cookies clicked away");

	}
	
	public void navigateToSurveyPage() {
		clickElement(a_zProductListing_link);
	}
	public void hederMenuClick (WebElement e) {
		clickElement(e);
	}
	public void search(String s) {
		setInput(search_input, s);
		clickElement(search_button);
	}

	public void naviagteViaMenuSikuli(String path, WebElement e) {
		// TODO Auto-generated method stub
		hover(path);
		clickElement(e);
	}
}
