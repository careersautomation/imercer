package pages.iMercer_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.iMercer_pages.MFA;

import static driverfactory.Driver.*;

public class Login {

	@FindBy(id = "ctl01_lblWelcome")
	public static WebElement WelcomeToiMercer;
	@FindBy(id = "lblSelectedRegion")
	public static WebElement RegionBanner;
	@FindBy(id = "myaccounts")
	WebElement MyAccountsLink;
	@FindBy(id = "Header1_iMercerHeader_iMercerMiniHeader_hyperlnkSignIn")
	WebElement SignInButtonToLoginPage;
	@FindBy(id = "ctl00_MainSectionContent_Email")
	WebElement EmailIdInput;
	@FindBy(id = "ctl00_MainSectionContent_Password")
	WebElement PasswordInput;
	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	WebElement SignInButton;
	@FindBy(id = "welcomeWithName")
	public static WebElement LoggedInUserName;

	@FindBy(id = "changeLocation")
	public static WebElement changeRegion_link;

	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aParentRegionLink'][contains(text(), 'Canada')]")
	public static WebElement canadaRegion_link;

	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aParentRegionLink'][contains(text(), 'Europe')]")
	public static WebElement europeRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aParentRegionLink'][contains(text(), 'United States')]")
	public static WebElement usRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aParentRegionLink'][contains(text(), 'Asia')]")
	public static WebElement asiaRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildRegionLink'][contains(text(), 'China')]")
	public static WebElement chinaRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildLanguageLink'][contains(text(), '中文')]")
	public static WebElement chinaChineseRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildRegionLink'][contains(text(), 'China')]/following-sibling::a[text() = 'English']")
	public static WebElement chinaEnglishRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildRegionLink'][contains(text(), '日本')]")
	public static WebElement japanRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildRegionLink'][contains(text(), 'Japan')]/following-sibling::a[text() = 'English']")
	public static WebElement japanEnglishRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildLanguageLink'][contains(text(), '日本語')]")
	public static WebElement japanJapaneseRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aParentRegionLink'][contains(text(), 'オーストラリア/NZ')]")
	public static WebElement australiaNZRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aParentRegionLink'][contains(text(), 'Latin America')]")
	public static WebElement latinAmericaRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildLanguageLink'][contains(text(), 'Português')]")
	public static WebElement portuguesRegion_link;

	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildLanguageLink'][contains(text(), 'Português')]/preceding-sibling::a[text() = 'English']")
	public static WebElement latinamericaEnglishRegion_link;
	
	@FindBy(xpath = "//ul[@id = 'LocationUL']/li/a[@class = 'aChildLanguageLink'][contains(text(), 'Español')]")
	public static WebElement latinAmericaEspanolRegion_link;
	
	@FindBy(xpath = "//li[text() = 'Go to My Account']")
	public static WebElement goToMyAccount_link;
	
	@FindBy(xpath = "//li[text() = 'Order History']")
	public static WebElement orderHistory_link;
	
	public Login() {
		PageFactory.initElements(driver, this);
	}

	public void regionSelection(String region) {
		clickElement(driver.findElement(By.partialLinkText(region)));
	}

	public void login(String username, String password) throws InterruptedException {
		hoverOverElement(driver, MyAccountsLink);
		clickElement(SignInButtonToLoginPage);
		setInput(EmailIdInput, username);
		setInput(PasswordInput, password);
		clickElement(SignInButton);
		delay(5000);
		if (driver.getPageSource().contains("Contact Us for Help")) {
			navigateBack(driver);
			setInput(PasswordInput, password + Keys.ENTER);
			delay(5000);
		}
	}

	public void modifyRegion(WebElement e) {
		hoverOverElement(driver, changeRegion_link);
		hoverAndClickOnElement(driver, changeRegion_link, e);
//		clickElement(e);
	}
	
	public void navigateToSurveyPageForEurope() {
		hoverOverElement(driver, MyAccountsLink);
		clickElement(goToMyAccount_link);
		delay(5000);
		switchToWindow("iMercer.com");
	}
	
	public void navigateToMyOrders() {
		hoverOverElement(driver, MyAccountsLink);
		clickElement(orderHistory_link);
		delay(5000);
		switchToWindow("iMercer > Order History");
	}
	

}


