package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IndustriesPage {
	
	@FindBy(id = "ctl03_repBoxes_ctl00_uxLayoutBoxPanel")
	public static WebElement industriesPage_content;
	
	@FindBy(xpath = "//h1[contains(text(), 'Test_US MTCS | Mercer Total Compensation Survey for the Energy Sector - Downstream and Oilfield Services')]")
	public static WebElement surveyTitle;
	
	
	 public IndustriesPage() {
		
		PageFactory.initElements(driver, this);
	}
}
