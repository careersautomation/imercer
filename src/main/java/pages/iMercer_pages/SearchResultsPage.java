package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class SearchResultsPage {

	@FindBy(id = "ctl03_txtSearch")
	public static WebElement searchKeywordInResult_input;
	
	@FindBy(id = "ctl03_ddlSubcollection")
	public static WebElement regionInResult_dropdown;
	
	public SearchResultsPage() {
		PageFactory.initElements(driver, this);
	}
}
