package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RewardsPage {

	@FindBy(id = "ctl03_repBoxes_ctl00_lnkTitle")
	public static WebElement rewardsPage_title;

	@FindBy(xpath = "//span[@id = 'ctl03_lblSurveyName'][contains(text(), 'SportsSurveyVijay')]")
	public static WebElement sportsVijaySurvey_title;

	
	
	public RewardsPage() {
		PageFactory.initElements(driver, this);
	}
}
