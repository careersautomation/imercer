package pages.iMercer_pages;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.iMercer_pages.MFA;

import static driverfactory.Driver.*;


public class WarningBoxPage {

	@FindBy(id = "hcCartAlert")
	public static WebElement cartAlert_dialog;
	
	@FindBy(id = "addtocartbuttonCCUser")
	public static WebElement addToCartAfterDelete_button;
	
	@FindBy(id = "hcBackbtn")
	public static WebElement checkoutFirst_button;
	public WarningBoxPage() {

		PageFactory.initElements(driver, this);
	}

	public void deleteCartAndProceed() {
		// TODO Auto-generated method stub
		clickElement(addToCartAfterDelete_button);
	}

	public void checkOutAndProceed() {

		// TODO Auto-generated method stub
		clickElement(checkoutFirst_button);
	}
}

