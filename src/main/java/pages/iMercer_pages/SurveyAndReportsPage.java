package pages.iMercer_pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SurveyAndReportsPage {
	
	
	@FindBy(xpath = "//span[contains(text(), 'SURVEYS AND REPORTS')]")
	public static WebElement surveyAndReports_header;
	
	@FindBy(xpath ="//a[contains(text(), 'dnn9_testing_ss')]")
	public static WebElement survey_link;
	
	public SurveyAndReportsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectSurvey() {
		clickElement(survey_link);;
	}
	
}
