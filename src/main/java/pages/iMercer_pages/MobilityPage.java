package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MobilityPage {

	@FindBy(id = "ctl03_repBoxes_ctl00_lnkTitle")
	public static WebElement mobilityPage_title;
	
	
	@FindBy(xpath = "//h1[contains(text(), 'June 6_Test_OnlineAccess-This is for testing the list in Cart')]")
	public static WebElement surveyTitle;
	
	
	public MobilityPage() {
		PageFactory.initElements(driver, this);
	}
}
