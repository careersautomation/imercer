package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ServicesPage {

	@FindBy(id = "ctl03_repBoxes_ctl00_lnkTitle")
	public static WebElement servicesPage_title;
	
	@FindBy(xpath = "//h1[contains(text(), 'Test Concurrents User_Apr 27AB')]")
	public static WebElement surveyTitle;
	
	public ServicesPage() {
		PageFactory.initElements(driver, this);
	}
}
