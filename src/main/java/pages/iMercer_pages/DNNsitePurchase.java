package pages.iMercer_pages;

import org.jfree.chart.axis.SegmentedTimeline;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import atu.testrecorder.media.quicktime.AbstractQuickTimeStream.Edit;

import static driverfactory.Driver.*;
import static driverfactory.Driver.delay;

import java.util.List;

public class DNNsitePurchase {
	@FindBy (id = "hcCCUserslnk")
	WebElement AddToCartButton;
	@FindBy (xpath = "//th[contains(text(),'SHOPPING CART')]")
	public static WebElement CartPageHeader;
	@FindBy (id="ShoppingCartCheckoutBtn")
	public static WebElement CheckOutButton;
	@FindBy (id = "specialinstructions")
	WebElement CommentBox;
	@FindBy (id = "openpayment")
	WebElement OpenPayment;
	@FindBy( xpath ="//input[@value='Invoice']")
	WebElement InvoiceRadioButton;
	@FindBy(id = "openorderdetail")
	WebElement OpenOrderDetail;
	@FindBy(id = "hcConfirmButton")
	WebElement SubmitOrderButton;
	@FindBy(xpath ="//span[text()='ORDER/PAYMENT INFORMATION']//following::span[2]")
	public WebElement OrderNumber;
	@FindBy(id = "divOrderReceipt")
	public WebElement OrderReceipt;
	@FindBy(id = "invoiceProducts")
	public WebElement OrderSummary;
	@FindBy(xpath = "//a[contains(text(), 'Edit item')]")
	public static WebElement editItem_button;
	
	@FindBy(xpath = "//ul[contains(@class,'cartProductDetailsText')]/li")
	public static WebElement priceSummary_text; 
	
	@FindBy(id = "lblTotalPriceShipping")
	public static WebElement totalAmount_label;
	
	
	@FindBy(xpath = "//input[@value = 'Remove']")
	public static WebElement removeItem_button;
	
	@FindBy(xpath = "//input[@value = 'Remove']")
	public static List <WebElement> allRemoveItem_button;

	@FindBy(xpath = "//h3[contains(text(), 'Your shopping cart is empty!')]")
	public static WebElement emptyCart_header;
	
	@FindBy(id = "spnCartItemCount1")
	public static WebElement itemCountInCart;
	
	@FindBy(xpath = "//a[contains(text(), 'Continue Shopping')]")
	public static WebElement continueToShopping;
	
	@FindBy(id = "couponcode")
	public static WebElement promotionalCode_input;
	
	@FindBy(id = "lblOSPromoCodeShipping_US21")
	public static WebElement promoCodeApplied_text;
	
	@FindBy(id = "submitcoupon")
	public static WebElement applyCode_button;
	
	public DNNsitePurchase() {
		PageFactory.initElements(driver, this);
	}
	public void addToCart() throws InterruptedException {
		clickElementUsingJavaScript(driver,AddToCartButton);
	}
	public void checkout() throws InterruptedException {
		clickElement(CheckOutButton);
		setInput(CommentBox,"Testing");
		clickElement(OpenPayment);
		clickElement(InvoiceRadioButton);
		clickElementUsingJavaScript(driver,OpenOrderDetail);
		clickElementUsingJavaScript(driver,SubmitOrderButton);
		System.out.println("Exec complete");
		
	}
	
	public void editItem() {
		clickElement(editItem_button);
	}
	
	public void removeItem() {
		for(WebElement e : allRemoveItem_button)
		clickElement(removeItem_button);
	}
	
	public void continueToShopping() {
		clickElement(continueToShopping);
	}
	
	public void checkoutTobillingInfo() {
		delay(2000);
		clickElement(CheckOutButton);
	}
	public void selectPromotionalCode() {
		// TODO Auto-generated method stub
		setInput(promotionalCode_input, "US21");
		clickElement(applyCode_button);
	}
}
