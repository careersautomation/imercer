package pages.smoke_iMercer_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class DNNsiteSurveys {
	@FindBy(id = "changeLocation")
	WebElement ChangeLocationDropdown;
	@FindBy(id = "lblSelectedRegion")
	public WebElement LocationLabel;
	@FindBy(id = "Header1_Repeater1_ctl01_lnkTabName")
	WebElement RewardsMenu;
	@FindBy(xpath = "//a[contains(text(),'Surveys & Repotrs')]")
	WebElement SurveysAndReportsOption;
	@FindBy(xpath = "//span[contains(text(),'SURVEYS AND REPORTS')]")
	public WebElement SurveysAndReportsPageHeader;
	@FindBy(id = "Span_compensation")
	WebElement ExpandCompensation;
	@FindBy(id = "Span_us-compensation")
	WebElement ExpandUS;
	@FindBy(id = "Span_department-function")
	WebElement ExpandDepartmentFunction;
	@FindBy(id = "Div_engineering-2")
	WebElement DepartmentFunction_Engineering;
	@FindBy(xpath = "//a[text()='Pre_Testing_HC']")
	WebElement PretestingHC;
	@FindBy(css = ".SurveyTitle")
	public WebElement SurveyTitle;
	@FindBy(id = "hcCCUserslnk")
	public WebElement AddToCartButton;

	@FindBy(id = "opt8d097dd829834238b0ccdb01535a157a")
	public WebElement selectQuantity_dropdown;

	@FindBy(id = "selectedQuantityPrice")
	public WebElement quantitySelected_message;

	@FindBy(id = "totalPrice")
	public WebElement totalPrice_text;

	@FindBy(id = "dnn_ctr563_ModuleContent")
	public WebElement surveyAndReports_headerTitle;

	@FindBy(xpath = "//a[contains(text(), 'dnn9_testing_ss')]")
	public WebElement survey_link;

	@FindBy(id = "ctl03_lnkProducts")
	WebElement AToZProductListingLink;

	public DNNsiteSurveys() {
		PageFactory.initElements(driver, this);
	}
 
	public void changeLocation(String region) throws InterruptedException {
		hoverOverElement(driver, ChangeLocationDropdown);
		clickElementUsingJavaScript(driver,driver.findElement(By.partialLinkText(region)));
		waitForElementToDisplay(LocationLabel);

	}

	public void open(String mainmenu, String submenu) throws InterruptedException {
		hoverOverElement(driver,driver.findElement(By.xpath("//span[contains(text(),'"+mainmenu+"')]")));
		clickElement(driver.findElement(By.xpath("//div[contains(@class,'subMenu')]//a[contains(text(),'"+submenu+"')]")));
	}

	public void selectSurvey(String menu, String submenu1, String submenu2, String submenu3, String survey)
			throws InterruptedException {
		clickElement(driver.findElement(By.xpath("//a[text()='" + menu + "']")));
		clickElement(driver.findElement(
				By.xpath("//li[contains(@id,'" + menu.toLowerCase() + "')]//a[text()='" + submenu1 + "']")));
		clickElement(driver.findElement(
				By.xpath("//li[contains(@id,'" + menu.toLowerCase() + "')]//a[text()='" + submenu2 + "']")));
		clickElementUsingJavaScript(driver, driver.findElement(
				By.xpath("//li[contains(@id,'" + menu.toLowerCase() + "')]//a[text()='" + submenu3 + "']")));
		clickElementUsingJavaScript(driver, driver.findElement(By.xpath("//a[text()='" + survey + "']")));
	}

	public void selectSurvey(String surveyCategory, String survey) throws InterruptedException {
		clickElement(driver.findElement(By.xpath(
				"//span[contains(text(),'SURVEYS AND REPORTS')]//following::a[text()='" + surveyCategory + "']")));
		clickElement(driver.findElement(
				By.xpath("//span[contains(text(),'SURVEYS AND REPORTS')]//following::a[text()='" + survey + "']")));
	}

	public void selectSurvey() {
		clickElement(survey_link);
	}

	public void selectSurveyQuantity(String value) {
		selEleByValue(selectQuantity_dropdown, value);
	}

	public void navigateToCartSummary() {
		// TODO Auto-generated method stub
		clickElement(AddToCartButton);

	}

}
