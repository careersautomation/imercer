package pages.smoke_iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;
import static driverfactory.Driver.setInput;

public class LegacyPurchase {
	@FindBy (id = "ctl03_imgBuyNow")
	WebElement BuyNowButton; 
	@FindBy(id = "ctl03_po_btnAdd2")
	WebElement AddToCartButton;
	@FindBy (id = "ctl03_txtTitle")
	WebElement TitleInput;
	@FindBy (id = "ctl03_imgBtnContinue")
	WebElement ContinueButton;
	@FindBy (id = "ctl03_imgBtnCheckout")
	WebElement CheckoutButton;
	@FindBy(id = "ctl03_ddlCountry")
	WebElement BillingCountry;
	@FindBy(id = "ctl03_lblHeader")
	public WebElement BillingInfoHeader;
	@FindBy (id = "ctl03_txtInfo")
	WebElement CommentBox;
	@FindBy(id = "ctl03_uxTxtShippingCUIT")
	WebElement CUIT;
	@FindBy(id = "ctl03_uxTxtShippingAccountContact")
	WebElement ShippingAccContact;
	@FindBy(id = "ctl03_uxTxtShippingAccountEmail")
	WebElement ShippingAccEmail;
	@FindBy (id = "ctl03_btnContinue2")
	WebElement ContinueButton2;
	@FindBy (id = "ctl03_uxInvoiceImageButton")
	WebElement InvoiceButton;
	@FindBy (id = "ctl03_lblHeader")
	public WebElement OrderReceipt;
	@FindBy(id = "ctl03_osOrder_pnlInvoiceDetails")
	public WebElement OrderSummary;
	@FindBy(xpath ="//span[text()='Order/Payment Information']//following::span[3]")
	public WebElement OrderNumber;
	
	public LegacyPurchase() {
		PageFactory.initElements(driver, this);
		
	}
	public void addToCart() throws InterruptedException {
		clickElement(BuyNowButton);
		clickElement(AddToCartButton);
		setInput(TitleInput,"Testing");
		clickElement(ContinueButton);
		clickElement(CheckoutButton);
		if(isElementExisting(driver,BillingCountry,10) == true) {
			selEleByVisbleText(BillingCountry,"Argentina");
			clickElement(CheckoutButton);
		}
		
	}
	public void checkout() throws InterruptedException {
//		if(isElementExisting(driver,CUIT,10) == true && CUIT.getText()=="") 
//			setInput(CUIT,"Testing");
//		if(isElementExisting(driver,ShippingAccContact,10) == true && ShippingAccContact.getText()=="") 
//			setInput(ShippingAccContact,"Testing");
//		if(isElementExisting(driver,ShippingAccEmail,10) == true && ShippingAccEmail.getText()=="") 
//			setInput(ShippingAccEmail,"testing@mercer.com");
		if(isElementExisting(driver,CommentBox,10) == true) 
			setInput(CommentBox,"Testing");
		clickElementUsingJavaScript(driver,ContinueButton2);
		clickElementUsingJavaScript(driver,ContinueButton2);
		clickElement(InvoiceButton);
	}
	
}
