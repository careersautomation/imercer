package pages.smoke_iMercer_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

import java.util.List;

public class DNNsitePurchase {
	@FindBy (xpath = "//a[contains(text(),'Add to Cart')]")
	WebElement AddToCartButton;
	@FindBy (xpath = "//th[contains(text(),'SHOPPING CART')]")
	public WebElement CartPageHeader;
	@FindBy (id="ShoppingCartCheckoutBtn")
	public WebElement CheckOutButton;
	@FindBy (id = "specialinstructions")
	WebElement CommentBox; 
	@FindBy (id = "openpayment") 
	   
	WebElement OpenPayment;
	@FindBy( xpath ="//input[@value='Invoice']")
	WebElement InvoiceRadioButton;
	@FindBy(id = "openorderdetail")
	WebElement OpenOrderDetail;
	@FindBy(id = "hcConfirmButton")
	WebElement SubmitOrderButton;
	@FindBy(xpath ="//span[text()='ORDER/PAYMENT INFORMATION']//following::span[2]")
	public WebElement OrderNumber;
	@FindBy(id = "divOrderReceipt")
	public WebElement OrderReceipt;
	@FindBy(id = "invoiceProducts")
	public WebElement OrderSummary;
	@FindBy(xpath = "//a[contains(text(), 'Edit item')]")
	public WebElement editItem_button;
	
	@FindBy(xpath = "//ul[contains(@class,'cartProductDetailsText')]/li")
	public WebElement priceSummary_text; 
	
	@FindBy(id = "lblTotalPriceShipping")
	public WebElement totalAmount_label;
	
	@FindBy(xpath = "//input[@value = 'Remove']")
	public WebElement removeItem_button;
	
	@FindBy(xpath = "//input[@value = 'Remove']")
	public List <WebElement> allRemoveItem_button;

	@FindBy(xpath = "//h3[contains(text(), 'Your shopping cart is empty!')]")
	public WebElement emptyCart_header;
	
	@FindBy(id = "spnCartItemCount")
	public WebElement itemCountInCart;
	
	@FindBy(xpath = "//a[contains(text(), 'Continue Shopping')]")
	public WebElement continueToShopping;
	
	@FindBy(id = "couponcode")
	public WebElement promotionalCode_input;
	
	@FindBy(id = "lblOSPromoCodeShipping_US21")
	public WebElement promoCodeApplied_text;
	
	@FindBy(id = "submitcoupon")
	public WebElement applyCode_button;
	
	@FindBy(xpath = "//a[contains(text(),'Buy Now')]")
	WebElement BuyNowButton;
	@FindBy(id ="shippingstate")
	WebElement StateProvinceDropdown;
	
	public DNNsitePurchase() {
		PageFactory.initElements(driver, this);
	}
	public void addToCart() throws InterruptedException {
		clickElement(BuyNowButton);
		clickElement(AddToCartButton);
	}
	public void checkout() throws InterruptedException {
		clickElement(CheckOutButton);
		waitForElementToEnable(StateProvinceDropdown);
		delay(3000);
		clickElementUsingJavaScript(driver,CommentBox);
		setInput(CommentBox,"Testing");
		clickElement(OpenPayment);
		clickElement(InvoiceRadioButton);
		clickElementUsingJavaScript(driver,OpenOrderDetail);
		clickElementUsingJavaScript(driver,SubmitOrderButton);		
	}
	
	public void editItem() {
		clickElement(editItem_button);
	}
	
//	public void removeItem() {
//		for(WebElement e : allRemoveItem_button)
//		clickElement(removeItem_button);
//	}
	
	public void continueToShopping() {
		clickElement(continueToShopping);
	}
	
	public void checkoutTobillingInfo() {
		clickElement(CheckOutButton);
	}
	public void selectPromotionalCode() {
		// TODO Auto-generated method stub
		setInput(promotionalCode_input, "US21");
		clickElement(applyCode_button);
	}
}
