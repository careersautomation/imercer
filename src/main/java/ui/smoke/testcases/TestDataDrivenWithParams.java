package ui.smoke.testcases;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.internal.Killable;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.DashBoardPage;
import pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TestDataDrivenWithParams extends InitTests {
	DashBoardPage homepage ;
	  WebDriver driver=null;
	  ExtentTest test=null;
	  Driver driverObj=new Driver();
	  
	@BeforeClass
	public void login() throws IOException  
	{
		try {
			test = reports.createTest("login");
			test.assignCategory("smoke");
			 WebDriver driver=driverObj.initWebDriver( BASEURL,"chrome","","","local",test, "");
			LoginPage loginPage = new LoginPage(driver);
			loginPage.login(USERNAME,PASSWORD);
		
		} 
			
		 catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();


		} 
	}
@Parameters({"country"})
	@Test(groups="smoke")
	public void testSearchJob(String country) throws Exception {
		try {
			test = reports.createTest("testSearchJob");
			test.assignCategory("smoke");
			homepage = new DashBoardPage(driver);
			homepage.searchJob(country);
			waitForElementToDisplay(DashBoardPage.resultTxt);
			verifyElementTextContains(DashBoardPage.resultTxt, "results",test);
			homepage.removeCountry();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
			reports.flush();
			driver.close();

		} 
		
	}
@AfterClass
public void logout() throws IOException  
{
	try
	{
		test = reports.createTest("logout");
		test.assignCategory("smoke");
		homepage.headerpage.logout();
	}
	catch (Exception e)
	{
	e.printStackTrace();
	SoftAssertions.fail(e,driverObj.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	softAssert.assertAll();


} 
finally
{
	reports.flush();
	driver.close();
	

}
}
	
}
