package ui.smoke.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;

import pages.smoke_iMercer_pages.*;
import verify.SoftAssertions;

public class Script3_DNN {
	@Test(priority = 1, enabled = true)
	public void DNNRegionPurchase() throws Exception {
		try {
			DNNsiteSurveys DNNsurveyobj = new DNNsiteSurveys();
			DNNsitePurchase DNNpurchaseobj = new DNNsitePurchase();
			UserEmail useremailobj = new UserEmail();
			
			test = reports.createTest("DNN region purchase");
			//DNNsurveyobj.changeLocation("United States");
			//verifyElementTextContains(DNNsurveyobj.LocationLabel,"United States",test);
			DNNsurveyobj.open("Rewards","Surveys & Repotrs");
		assertTrue(isElementExisting(driver,DNNsurveyobj.SurveysAndReportsPageHeader,20),"Surveys And Reports page has been loaded",test);
		DNNsurveyobj.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			verifyElementTextContains(DNNsurveyobj.SurveyTitle,"Pre_Testing_HC",test);
		
		DNNpurchaseobj.addToCart();
		assertTrue(isElementExisting(driver,DNNpurchaseobj.CartPageHeader,20),"Shopping Cart page has loaded successfully",test);
		DNNpurchaseobj.checkout();
		assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderReceipt,20),"Order Receipt page has loaded",test);
		assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderNumber,20),"Order Number is generated and displayed",test);
		assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderSummary,20),"Order Information is displayed ",test);
			
		useremailobj.openOutlookEmail();
		assertTrue(isElementExisting(driver,useremailobj.MessageSenderDNN,20),"Order invoice email has been verified",test);
		useremailobj.switchToMainWindow();
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

								} 
		}
}
