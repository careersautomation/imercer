package ui.smoke.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.smoke_iMercer_pages.*;
import verify.SoftAssertions;
import utilities.InitTests;

public class Script1_Login extends InitTests {
	public static String MFAChoice = "";
	public static String MFAEmailID = "";
	public static String MFAEmailPassword = "";

	public Script1_Login(String appName) {
		super(appName);
	}
	
	@BeforeMethod
	public void setUp() throws Exception {
		props.load(input);
		MFAChoice = props.getProperty("MFAChoiceiMercer");
		MFAEmailID = props.getProperty("MFAEmailId");
		MFAEmailPassword = props.getProperty("MFAEmailPassword");
		test = reports.createTest("Logging in to iMercer");
		test.assignCategory("Regression");
		
	}
	@Test(priority = 1, enabled = true)
	public void LegacyRegionPurchase() throws Exception {
		try {
			test = reports.createTest("Logging in to iMercer");
			test.assignCategory("smoke");
			@SuppressWarnings("unused")
			Script1_Login iMercerobj = new Script1_Login("iMercer");
			initWebDriver(BASEURL, "CHROME", "latest","","local", test, "");
			System.out.println("email:  "+MFAEmailID + "password : "+MFAEmailPassword);
			Login loginobj = new Login();
			assertTrue(isElementExisting(driver,Login.WelcomeToiMercer,20),"Imercer site has loaded successfully",test);
			loginobj.regionSelection("United States");
			verifyElementTextContains(Login.RegionBanner,"United States",test);
			loginobj.login(USERNAME, PASSWORD);
			MFA mfa = new MFA();
			mfa.authenticate(MFAEmailID, MFAEmailPassword, MFAChoice);
			assertTrue(isElementExisting(driver,Login.LoggedInUserName,20),"User login Successful",test);
				
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

} catch (Exception e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

						} 
}
@AfterSuite
public void kill() {
reports.flush();
driver.quit();
killBrowserExe(BROWSER_TYPE);
}
}
