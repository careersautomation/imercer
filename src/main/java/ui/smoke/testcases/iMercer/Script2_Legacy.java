package ui.smoke.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import pages.iMercer_pages.LegacyPurchase;
import pages.iMercer_pages.LegacySurvey;
import pages.iMercer_pages.UserEmail;
import utilities.InitTests;
import verify.SoftAssertions;

public class Script2_Legacy extends InitTests {

	public Script2_Legacy(String appName) {
		super(appName);
	}
	
	//Legacy Region is no more in Use
	@Test(priority = 1, enabled = false)
	public void LegacyRegionPurchase() throws Exception {
		try {
			test = reports.createTest("Legacy region purchase");

			LegacySurvey surveyobj = new LegacySurvey();
			LegacyPurchase purchaseobj = new LegacyPurchase();
			UserEmail useremailobj = new UserEmail();

			surveyobj.selectSurvey("Club Oil Gas Colombia English Test");
			verifyElementTextContains(surveyobj.SportsSurveyVijayPageHeader, "Club Oil Gas Colombia English Test",
					test);

			purchaseobj.addToCart();
			verifyElementTextContains(purchaseobj.BillingInfoHeader, "Shipping & Billing Information", test);

			purchaseobj.checkout();
			assertTrue(isElementExisting(driver, purchaseobj.OrderReceipt, 20),
					"Order Details page loaded successfully", test);
			assertTrue(isElementExisting(driver, purchaseobj.OrderNumber, 20),
					"Order Number is generated and displayed", test);
			assertTrue(isElementExisting(driver, purchaseobj.OrderSummary, 20), "Order information is displayed", test);

			useremailobj.openOutlookEmail();
			assertTrue(isElementExisting(driver, useremailobj.MessageSenderLegacy, 20),
					"Order invoice email has been verified", test);
			useremailobj.switchToMainWindow();
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);

		}
	}

	@AfterSuite
	public void kill() {
		reports.flush();
		driver.quit();
		killBrowserExe(BROWSER_TYPE);
	}
}
