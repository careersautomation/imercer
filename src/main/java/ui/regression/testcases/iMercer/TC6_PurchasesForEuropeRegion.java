package ui.regression.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.switchToWindow;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.BillingInformationPage;
import pages.iMercer_pages.DNNsitePurchase;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.HeaderMenuPage;
import pages.iMercer_pages.Login;
import pages.iMercer_pages.OutlookMailPage;
import pages.iMercer_pages.PaymentInformationPage;
import pages.iMercer_pages.ReceiptPage;
import pages.iMercer_pages.ReviewOrderPage;
import verify.SoftAssertions;

public class TC6_PurchasesForEuropeRegion {
public static String vatNumber = "123456";
	@Test(enabled = true, priority = 1)
	public void changeTheRegionToEurope () throws Exception {
		try {
			
			test = reports.createTest("Switch the region to Europe");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.europeRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Europe", test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to Europe FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to Europe FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	

	@Test(enabled = true, priority = 2)
	public void verifyPurchaseFor_DenmarkOilSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verify details for Denmark Oil Survey");
			test.assignCategory("regression");
			
			Login loginPage = new Login();
			loginPage.navigateToSurveyPageForEurope();
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			assertTrue(isElementExisting(driver, HeaderMenuPage.a_zProductListing_link, 5), "Navigated to my account page successfully after switching the window",test);
			headerMenuPage.navigateToSurveyPage();
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.narrowSurveyList_text, 5), "Survey List is narrowed as per category",test);

			surveyPage.selectSurvey(DNNsiteSurveys.EuropeDenmarkOilSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.buyNow();
			surveyPage.denmarkOilEnterQuantity("2");
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("2 PDF @ EUR 1,500.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("EUR 3,000.00"), "Price total label is displayed on the page",test);
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.vat_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.enterVATnumber(vatNumber);
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "EUR", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify details for Denmark Oil Survey", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify details for Denmark Oil Survey", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	

	@Test(enabled = true, priority = 3)
	public void VATNumberPrepopulation () throws Exception {
		try {
			
			test = reports.createTest(" Verify previously entered VAT number");
			test.assignCategory("regression");
			
			Login loginPage = new Login();
			loginPage.navigateToSurveyPageForEurope();
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			assertTrue(isElementExisting(driver, HeaderMenuPage.a_zProductListing_link, 5), "Navigated to my account page successfully after switching the window",test);
			headerMenuPage.navigateToSurveyPage();
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.narrowSurveyList_text, 5), "Survey List is narrowed as per category",test);

			surveyPage.selectSurvey(DNNsiteSurveys.EuropeDenmarkOilSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.buyNow();
			surveyPage.denmarkOilEnterQuantity("3");
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("3 PDF @ EUR 1,500.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("EUR 4,500.00"), "Price total label is displayed on the page",test);
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.vat_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
//			billingInfoPage.fillData("NY");
			assertTrue((BillingInformationPage.vat_input.getAttribute("value")).trim().equals(vatNumber), "Previously entered VAT number is present",test);			
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "EUR", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify previously entered VAT number", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify previously entered VAT number", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 4)
	public void deleteVATNumber () throws Exception {
		try {
			
			test = reports.createTest("Delete VAT number entered earlier");
			test.assignCategory("regression");
			
			Login loginPage = new Login();
			loginPage.navigateToSurveyPageForEurope();
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			assertTrue(isElementExisting(driver, HeaderMenuPage.a_zProductListing_link, 5), "Navigated to my account page successfully after switching the window",test);
			headerMenuPage.navigateToSurveyPage();
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.narrowSurveyList_text, 5), "Survey List is narrowed as per category",test);

			surveyPage.selectSurvey(DNNsiteSurveys.EuropeDenmarkOilSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.buyNow();
			surveyPage.denmarkOilEnterQuantity("4");
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("4 PDF @ EUR 1,500.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("EUR 6,000.00"), "Price total label is displayed on the page",test);
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.vat_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.enterVATnumber("");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234","EUR", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Delete VAT number entered earlier", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Delete VAT number entered earlier", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 5)
	public void deleteVATNumberVerification () throws Exception {
		try {
			
			test = reports.createTest("VAT number deleted in previous scenario is deleted properly and does not prepopulate");
			test.assignCategory("regression");
			
			Login loginPage = new Login();
			loginPage.navigateToSurveyPageForEurope();
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			assertTrue(isElementExisting(driver, HeaderMenuPage.a_zProductListing_link, 5), "Navigated to my account page successfully after switching the window",test);
			headerMenuPage.navigateToSurveyPage();
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.narrowSurveyList_text, 5), "Survey List is narrowed as per category",test);

			surveyPage.selectSurvey(DNNsiteSurveys.EuropeDenmarkOilSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.buyNow();
			surveyPage.denmarkOilEnterQuantity("4");
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("4 PDF @ EUR 1,500.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("EUR 6,000.00"), "Price total label is displayed on the page",test);
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.vat_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			//check if this returns NULL or empty string
			assertTrue((BillingInformationPage.vat_input.getAttribute("value")).equals(""), "VAT number was deleted and did not re populated",test);
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "EUR", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to validate if vat number deleted earlier is deleted completely", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to validate if vat number deleted earlier is deleted completely", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
}
