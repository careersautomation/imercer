package ui.regression.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.BillingInformationPage;
import pages.iMercer_pages.DNNsitePurchase;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.HeaderMenuPage;
import pages.iMercer_pages.PaymentInformationPage;
import pages.iMercer_pages.PaypalOrderPage;
import pages.iMercer_pages.ReceiptPage;
import pages.iMercer_pages.ReviewOrderPage;
import verify.SoftAssertions;

public class TC3_PlaceOrderUsingVariousPaymentMethods {

	@Test(enabled = true, priority = 1)
	public void placeOrderUsingInvoiceMethod() throws Exception {
		try {
			
			test = reports.createTest("Complete the procedure to place order INVOICE METHOD");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 100.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@Test(enabled = true, priority = 2)
	public void placeOrderUsingCreditCardMethod() throws Exception {
		try {
			
			test = reports.createTest("Complete the procedure to place order CREDIT CARD");
			test.assignCategory("regression");
			
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 100.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.creditCart_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			//add code for paypal
			PaypalOrderPage payPalPage = new PaypalOrderPage();
			assertTrue(isElementExisting(driver, PaypalOrderPage.email_input, 20), "Navigated to paypal page for completing payment",test);
			
			payPalPage.loginToPayPal();
			payPalPage.completePayment();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order CREDIT CARD", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order CREDIT CARD", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 3) 
	public void placeOrderUsingInvoiceMethodWithPromotionalCode() throws Exception {
		try {
			
			test = reports.createTest("Complete the procedure to place order INVOICE METHOD : PROMOTIONAL CODE");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			

			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@Test(enabled = true, priority = 4)
	public void placeOrderUsingCreditCardMethodPromotionalCode() throws Exception {
		try {
			
			test = reports.createTest("Complete the procedure to place order CREDIT CARD : PROMOTIONAL CODE");
			test.assignCategory("regression");
			
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 100.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.creditCart_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			//add code for paypal
//			assertTrue(isElementExisting(driver, PaypalOrderPage.email_input, 5), "Navigated to paypal page for completing payment",test);
//			
			
			PaypalOrderPage payPalPage = new PaypalOrderPage();
			payPalPage.loginToPayPal();
			payPalPage.completePayment();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order CREDIT CARD : PROMOTIONAL CODE", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order CREDIT CARD : PROMOTIONAL CODE", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 5)
	public void placeOrderUsingInvoiceMethodWithTaxExemption() throws Exception {
		try {
			
			test = reports.createTest("Complete the procedure to place order INVOICE METHOD : PROMOTIONAL CODE : TAX EXEMPTION");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			

			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
//			billingInfoPage.fillData("NY");
			billingInfoPage.selectTaxExempt();
			assertTrue(isElementExisting(driver, BillingInformationPage.taxExempt_label, 5), "Tax exempt is applied to the order value",test);
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE  : TAX EXEMPTION", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE : TAX EXEMPTION", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 6)
	public void placeOrderUsingInvoiceMethodWithTax() throws Exception {
		try {
			
			test = reports.createTest("Complete the procedure to place order INVOICE METHOD : PROMOTIONAL CODE : NO TAX EXEMPTION : Tax is applied for that region");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			

			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.deSelectTaxExempt();
			assertTrue(isElementExisting(driver, BillingInformationPage.taxValue_label, 5), "Tax is applied to the order value",test);
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE  : NO TAX EXEMPTION : Tax is applied for that region", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE : NO TAX EXEMPTION : Tax is applied for that region", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 7)
	public void placeOrderUsingInvoiceMethodWithoutTax() throws Exception {
		try {
			
			test = reports.createTest("Complete the procedure to place order INVOICE METHOD : PROMOTIONAL CODE : NO TAX EXEMPTION : Tax is not applied for that region");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			

			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("OH");
			billingInfoPage.deSelectTaxExempt();
			assertTrue(isElementExisting(driver, BillingInformationPage.zeroTaxValue_label, 5), "Tax is not applied to the order value as region is OHIO",test);
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE  : NO TAX EXEMPTION : Tax is not applied for that region", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to place order INVOICE METHOD : PROMOTIONAL CODE : NO TAX EXEMPTION : Tax is not applied for that region", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 8)
	public void navigateBackToDeliveryInfoPageInvoiceMethod() throws Exception {
		try {
			
			test = reports.createTest("Navigate to billing information from payment page : INVOICE METHOD");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			

			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.deSelectTaxExempt();
			assertTrue(isElementExisting(driver, BillingInformationPage.taxValue_label, 5), "Tax is  applied to the order value as region is NEW YORK",test);
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			
			paymentInfoPage.navigateToDeliveryInfoStep();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to delivery information page",test);
			billingInfoPage.continueToNextStep();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Navigate to billing information from payment page : INVOICE METHOD", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Navigate to billing information from payment page : INVOICE METHOD", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 9)
	public void navigateBackToDeliveryInfoPageCreditCard() throws Exception {
		try {
			
			test = reports.createTest("Navigate to billing information from payment page : CC");
			test.assignCategory("regression");
			
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 100.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			
			paymentInfoPage.navigateToDeliveryInfoStep();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to delivery information page",test);
			billingInfoPage.continueToNextStep();
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.creditCart_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			//add code for paypal
			PaypalOrderPage payPalPage = new PaypalOrderPage();
//			assertTrue(isElementExisting(driver, PaypalOrderPage.email_input, 5), "Navigated to paypal page for completing payment",test);
//			
			payPalPage.loginToPayPal();
			payPalPage.completePayment();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("failed Navigate to billing information from payment page : CC", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed Navigate to billing information from payment page : CC", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 10)
	public void verifyReurnPolicy() throws Exception {
		try {
			
			test = reports.createTest("verify return policy while placing order");
			test.assignCategory("regression");
			
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 100.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.selectPromotionalCode();
			assertTrue(isElementExisting(driver, DNNsitePurchase.promoCodeApplied_text, 5), "Promotional code is applied successfully to the cart",test);
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			// verifying return policy
			
			String returnPolicyText_expected = "Returns must be received within 30 days of notification of product availability. Mercer will provide a refund of fees paid for product(s) that have not been accessed. No fees will be refunded for accessed products or products returned past 30 days of notification of product availability.";
			
			String returnPolicyText_actual = billingInfoPage.getReturnPolicy();
			assertTrue(returnPolicyText_actual.equals(returnPolicyText_expected), "Return policy is verified",test);			
			
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			
			paymentInfoPage.navigateToDeliveryInfoStep();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to delivery information page",test);
			billingInfoPage.continueToNextStep();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("failed to view return policy", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to view return policy", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

}
