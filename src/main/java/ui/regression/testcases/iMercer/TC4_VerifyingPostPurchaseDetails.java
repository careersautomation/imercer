package ui.regression.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyEquals;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.BillingInformationPage;
import pages.iMercer_pages.DNNsitePurchase;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.HeaderMenuPage;
import pages.iMercer_pages.OutlookMailPage;
import pages.iMercer_pages.PaymentInformationPage;
import pages.iMercer_pages.ReceiptPage;
import pages.iMercer_pages.ReviewOrderPage;
import verify.SoftAssertions;

public class TC4_VerifyingPostPurchaseDetails {
	
	@Test(enabled = true, priority = 1)
	public void onlineAccessSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of  'Online Access' Product Purchase");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","dnn9_testing_oa");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Online Access @ USD 137.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 137.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of  'Online Access' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of  'Online Access' Product Purchase failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	

	@Test(enabled = true, priority = 2)
	public void hardCopySurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'Hard Copy' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","db_testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 100.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Hard Copy' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Hard Copy' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 3)

	public void concurrentUserSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'Concurrent User' Product Purchase");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","test_US_Cu1");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.selectParticipant();
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
//			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 30.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Concurrent User' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Concurrent User' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 4)

	public void concurrentUserWithAdditionalSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'Concurrent user with additional user' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_CU");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.selectParticipant1();
			assertTrue(getElementText(DNNsiteSurveys.totalAmountLabel_concurrentUserSurvey).equals("USD 400.00"), "Price total label is displayed on the page",test);
			surveyPage.selectAdditionalUsers("76538d33fb2046d1a05ec12862ae7ec6");
			assertTrue(getElementText(DNNsiteSurveys.totalAmountLabel_concurrentUserSurvey).equals("USD 420.00"), "Price total label is displayed on the page",test);
		
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 420.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Concurrent user with additional user' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Concurrent user with additional user' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 5)
	public void colSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'COL/QOL' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_COL/QOL");
			surveyPage.selectHostAndBaseRegion("7247a31311bc4973b4a58a8b0e5a75ea","2c7d88cd-6f05-4a33-bb90-3a6931816db7","25d8ad1ddb2d4208ab467050011985a3");
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
//			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 400.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'COL/QOL' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'COL/QOL' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 6)
	public void slmsSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'Single level multi selection ' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","dnn9_testing_wiz");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.selectMultiSurvey();
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
//			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 250.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Single level multi selection' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Single level multi selection' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 7)
	public void slmsAdditionalUserSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'single level mlti selection additional user' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Executive","Test_US_SLMS_1");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.selectAdditionalUserSLMS();
			surveyPage.selectMultiSurveySLMS();
			
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
//			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
//			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 595.00"), "Price total label is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 620.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Feb@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'single level mlti selection additional user' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'single level mlti selection additional user' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 8)
	public void mlmsSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'multi level multi user' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Executive","US_3_10_MLMS");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.selectAreYouParticipant("e8718379f5624db1ba8edc0ff7fdf8ce");
			surveyPage.selectMultiSurveyMLMS();
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
//			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 275.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Feb@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'multi level multi user' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'multi level multi user' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 9)
	public void mlmsAdditionalUserSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'multi level mlti selection additional user' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","dnn9_testing_wiz_drop_addn");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.buyNow();
			surveyPage.selectAreYouParticipant1("c9be453728ac48a5a5103ea2041cae3d");
			surveyPage.selectAdditionalUserMLMS();
			surveyPage.selectMultiSurveyMLMS1();
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
//			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 245.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Feb@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'multi level mlti selection additional user' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'multi level mlti selection additional user' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 10)
	public void hardcopyAdvanceUserSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verification of 'Hardcopy advance' Product Purchase ");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HCA");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			
			surveyPage.selectMultiSurveyHardcopy();
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
//			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 Hard Copy @ USD 100.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 1,000.00"), "Price total label is displayed on the page",test);
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Feb@1234", "USD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Hardcopy advance' Product Purchase FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of 'Hardcopy advance' Product Purchase  failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
}
