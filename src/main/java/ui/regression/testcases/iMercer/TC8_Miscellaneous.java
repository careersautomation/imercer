package ui.regression.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.BillingInformationPage;
import pages.iMercer_pages.DNNsitePurchase;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.HeaderMenuPage;
import pages.iMercer_pages.Login;
import pages.iMercer_pages.OrderHistoryPage;
import pages.iMercer_pages.OutlookMailPage;
import pages.iMercer_pages.PaymentInformationPage;
import pages.iMercer_pages.ReceiptPage;
import pages.iMercer_pages.ReviewOrderPage;
import pages.iMercer_pages.SearchResultsPage;
import pages.iMercer_pages.WarningBoxPage;
import verify.SoftAssertions;

public class TC8_Miscellaneous {
 int orderIDReference;
	@Test(enabled = true, priority = 1)
	public void changeTheRegionToUS () throws Exception {
		try {
			
			test = reports.createTest("Switch region to US");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.usRegion_link);
			verifyElementTextContains(Login.RegionBanner, "United States", test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to US FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to US FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 2)
	public void verifyRelatedProductsList () throws Exception {
		try {
			
			test = reports.createTest("Verification of related products in survey listing page");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			surveyPage.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.relatedSurvey_title, 5), "Navigated to selected surveyproduct buy page and related survey section is present",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.nextSurvey_button, 5), "Next button is present",test);
			surveyPage.GoToNextSuggestion();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.previousSurvey_button, 5), "Previous button is present",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.june7TestSurvey_link, 5), "Survey link is present",test);
			surveyPage.GoToPreviousSuggestion();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.june6TestSurvey_link, 5), "Survey link is present",test);
			surveyPage.selectSurveyFromSuggestion();
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of related products in survey listing pages FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification of related products in survey listing page FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	

	@Test(enabled = true, priority = 3)
	public void modifyVariousRegions () throws Exception {
		try {
			
			test = reports.createTest("Switch to multiple regions and verify");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.asiaRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Asia", test);
			
			loginPage.modifyRegion(Login.chinaRegion_link);
			verifyElementTextContains(Login.RegionBanner, "China", test);
			
			loginPage.modifyRegion(Login.chinaEnglishRegion_link);
			verifyElementTextContains(Login.RegionBanner, "China", test);
			
			loginPage.modifyRegion(Login.chinaChineseRegion_link);
			verifyElementTextContains(Login.RegionBanner, "中国", test);
			
			loginPage.modifyRegion(Login.japanRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Japan", test);
			
			loginPage.modifyRegion(Login.japanEnglishRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Japan", test);
			
			loginPage.modifyRegion(Login.japanJapaneseRegion_link);
			verifyElementTextContains(Login.RegionBanner, "日本", test);
			
			loginPage.modifyRegion(Login.australiaNZRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Australia/NZ", test);
			
			loginPage.modifyRegion(Login.canadaRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Canada", test);
			
			loginPage.modifyRegion(Login.europeRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Europe", test);
			
			loginPage.modifyRegion(Login.latinAmericaRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Latin America", test);
			
			loginPage.modifyRegion(Login.portuguesRegion_link);
			verifyElementTextContains(Login.RegionBanner, "América Latina", test);

			loginPage.modifyRegion(Login.latinamericaEnglishRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Latin America", test);

			loginPage.modifyRegion(Login.latinAmericaEspanolRegion_link);
			verifyElementTextContains(Login.RegionBanner, "América Latina", test);

			loginPage.modifyRegion(Login.latinamericaEnglishRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Latin America", test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region  FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching regionFAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	@Test(enabled = true, priority = 4)
	public void verifySearchFunction () throws Exception {
		try {
			
			test = reports.createTest("Verify search function");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.usRegion_link);
			verifyElementTextContains(Login.RegionBanner, "United States", test);
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.search("Benefits");
			
			SearchResultsPage searchResultPage = new SearchResultsPage();
			assertTrue(SearchResultsPage.searchKeywordInResult_input.getAttribute("value").trim().equals("Benefits"), "Keyword is present",test);
			Select select1 =new Select(SearchResultsPage.regionInResult_dropdown);
			assertTrue(select1.getFirstSelectedOption().getText().trim().equals("United States"), "selected region United States is present",test);
			
			loginPage.modifyRegion(Login.europeRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Europe", test);
		
			headerMenuPage.search("Benefits");
			assertTrue(SearchResultsPage.searchKeywordInResult_input.getAttribute("value").trim().equals("Benefits"), "Keyword is present",test);
			Select select2 =new Select(SearchResultsPage.regionInResult_dropdown);
			assertTrue(select2.getFirstSelectedOption().getText().trim().equals("Europe"), "selected region Europe is present",test);
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verify search function FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verify search function FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}	
	
	@Test(enabled = true, priority = 5)
	public void multiCurrencyCheckoutDeleteCart () throws Exception {
		try {
			
			test = reports.createTest("Multi currency cart checkout (Delete Cart)");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.usRegion_link);
			verifyElementTextContains(Login.RegionBanner, "United States", test);
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
					
			surveyPage.selectSurvey(DNNsiteSurveys.dnnSurvey_link);
				
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.selectSurveyQuantity("de76a5b4a3bd41ea845082de43c844c3");
			//assertTrue((getElementText(DNNsiteSurveys.quantitySelected_message)).contains("You selected 100 copies at USD 4.00 each"), "Message for selected copies and its pricing is displayed.",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.totalPrice_text, 5), "Total cost for the surveys is displayed beside the Buy Now button",test);
			
			surveyPage.navigateToCartSummary();
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 30), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			loginPage.modifyRegion(Login.canadaRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Canada", test);
		
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			clickElement(DNNsiteSurveys.engineeringCanada_link);
			surveyPage.selectSurvey(DNNsiteSurveys.globalTestingPDFSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			WarningBoxPage warningBoxPage = new WarningBoxPage();
			assertTrue(isElementExisting(driver, WarningBoxPage.cartAlert_dialog, 5), "Alert box displayed",test);
			warningBoxPage.deleteCartAndProceed();
			
		
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 30), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("1 PDF @ CAD 60.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("CAD 60.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
		
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "CAD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Multi currency cart checkout (Delete Cart) FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Multi currency cart checkout (Delete Cart) FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 6)
	public void multiCurrencyCheckoutcheckoutProceed () throws Exception {
		try {
			
			test = reports.createTest("Multi currency cart checkout (checkout and proceed)");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.usRegion_link);
			verifyElementTextContains(Login.RegionBanner, "United States", test);
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
					
			surveyPage.selectSurvey(DNNsiteSurveys.dnnSurvey_link);
				
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.selectSurveyQuantity("de76a5b4a3bd41ea845082de43c844c3");
			//assertTrue((getElementText(DNNsiteSurveys.quantitySelected_message)).contains("You selected 100 copies at USD 4.00 each"), "Message for selected copies and its pricing is displayed.",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.totalPrice_text, 5), "Total cost for the surveys is displayed beside the Buy Now button",test);
			
			surveyPage.navigateToCartSummary();
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 30), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			loginPage.modifyRegion(Login.canadaRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Canada", test);
		
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			clickElement(DNNsiteSurveys.engineeringCanada_link);
			surveyPage.selectSurvey(DNNsiteSurveys.globalTestingPDFSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.navigateToCartSummary();
			
			WarningBoxPage warningBoxPage = new WarningBoxPage();
			assertTrue(isElementExisting(driver, WarningBoxPage.cartAlert_dialog, 5), "Alert box displayed",test);
			warningBoxPage.checkOutAndProceed();
			
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 30), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("100 copies @ 4.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 400.00"), "Price total label is displayed on the page",test);
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
		
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
		
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "USD", test));
			orderIDReference = actualOrderID;
			verifyEquals(actualOrderID, expectedOrderId, test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Multi currency cart checkout (checkout and proceed) FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Multi currency cart checkout (checkout and proceed) FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 7)
	public void verifyMyOrders () throws Exception {
		try {
			
			test = reports.createTest("verify list of my order");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.navigateToMyOrders();
			OrderHistoryPage orderHistoryPage = new OrderHistoryPage();
			assertTrue(isElementExisting(driver, OrderHistoryPage.orderTitle, 5), "Navigated to  order history page",test);
			verifyEquals(Integer.parseInt(OrderHistoryPage.orderID_text.getText().trim()), orderIDReference, test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verify list of my order FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verify list of my order FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
}
