package ui.regression.testcases.iMercer;

import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static driverfactory.Driver.killBrowserExe;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.HeaderMenuPage;
import pages.iMercer_pages.Login;
import pages.iMercer_pages.MFA;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_LoginTo_iMercer extends InitTests {
	public static String MFAChoice = "";
	static String MFAEmailID ="";
	static String MFAEmailPassword = "";

	public TC1_LoginTo_iMercer(String appName) {
		super(appName);
	}

	@Test(enabled = true , priority = 1)
	public void loginToIMercer() throws Exception {
		try {
			props.load(input);
			MFAChoice = props.getProperty("MFAChoiceiMercer");
			MFAEmailID = props.getProperty("MFAEmailId");
			MFAEmailPassword = props.getProperty("MFAEmailPassword");
			test = reports.createTest("Logging in to iMercer");
			test.assignCategory("Regression");
			
			TC1_LoginTo_iMercer iMercerRegression = new TC1_LoginTo_iMercer("iMercer");
			initWebDriver(BASEURL, "CHROME", "latest", "", "local", test, "");
			System.out.println("email:  "+MFAEmailID + "password : "+MFAEmailPassword);
			Login loginobj = new Login();
			assertTrue(isElementExisting(driver, Login.WelcomeToiMercer, 20), "Imercer site has loaded successfully",test);
			loginobj.regionSelection("United States");
			verifyElementTextContains(Login.RegionBanner, "United States", test);
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.acceptcookie();
			loginobj.login(USERNAME, PASSWORD);
			MFA mfa = new MFA();
			mfa.authenticate(MFAEmailID, MFAEmailPassword, MFAChoice);
			delay(6000);
			assertTrue(isElementExisting(driver, Login.LoggedInUserName, 20), "User login Successful", test);
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login to iMercer failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login to iMercer failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	@AfterSuite
	public void tearDown() {
		reports.flush();
		killBrowserExe("CHROME");
	}

}
