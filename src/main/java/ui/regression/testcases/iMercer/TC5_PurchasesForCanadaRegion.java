package ui.regression.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.BillingInformationPage;
import pages.iMercer_pages.DNNsitePurchase;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.HeaderMenuPage;
import pages.iMercer_pages.Login;
import pages.iMercer_pages.OutlookMailPage;
import pages.iMercer_pages.PaymentInformationPage;
import pages.iMercer_pages.ReceiptPage;
import pages.iMercer_pages.ReviewOrderPage;
import verify.SoftAssertions;

public class TC5_PurchasesForCanadaRegion {

	@Test(enabled = true, priority = 1)
	public void changeTheRegionToCanada () throws Exception {
		try {
			
			test = reports.createTest("Switch the region to Canada");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.canadaRegion_link);
			verifyElementTextContains(Login.RegionBanner, "Canada", test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to Canada FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to Canada FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	

	
	@Test(enabled = true, priority = 2)
	public void verifypurchaseFor_Canada_HCA () throws Exception {
		try {
			
			test = reports.createTest("Verify details for Canada_HCA survey");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReportsCanada_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.narrowSurveyList_text, 5), "Survey List is narrowed as per category",test);
			
			surveyPage.selectSurveyCategory(DNNsiteSurveys.ResearchAndInsightsSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.researchAndInsight_arrowLink, 5), "Arrow link is present for Research and Insights menu",test);
			

			surveyPage.selectSurvey(DNNsiteSurveys.canadaHCA_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.buyNow();
			surveyPage.navigateToCartSummary();
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.invalidSelection_errormessage, 5), "Error is displayed as no option is selected from the dropdowns",test);
			surveyPage.selectQuantityCanada(DNNsiteSurveys.surveyReportAt100_dropdown, "2833a21ec2794144889e67a316e21384");
			surveyPage.selectQuantityCanada(DNNsiteSurveys.surveyReportAt50_dropdown, "63026f0f6d8c4a09b46ba32737060be5");
			
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("CAD 350.00"), "Price total label is displayed on the page",test);
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "CAD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify details for Canada_HCA survey", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify details for Canada_HCA survey", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 3)
	public void verifypurchaseFor_Canada_CU () throws Exception {
		try {
			
			test = reports.createTest("Verify details for Canada_CU survey");
			test.assignCategory("regression");
			
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuCanada, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReportsCanada_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.narrowSurveyList_text, 5), "Survey List is narrowed as per category",test);
			
			surveyPage.selectSurveyCategory(DNNsiteSurveys.ResearchAndInsightsSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.researchAndInsight_arrowLink, 5), "Arrow link is present for Research and Insights menu",test);
			

			surveyPage.selectSurvey(DNNsiteSurveys.canadaCU_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			surveyPage.buyNow();
			surveyPage.selectAdditionalUserCanadaCU();
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("CAD 270.00"), "Price total label is displayed on the page",test);
			assertTrue(isElementExisting(driver, DNNsitePurchase.CheckOutButton, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			purchasePage.checkoutTobillingInfo();
			
			BillingInformationPage billingInfoPage = new BillingInformationPage();
			assertTrue(isElementExisting(driver, BillingInformationPage.firstName_input, 5), "Navigated to billing page after clicking on checkout button",test);
			
			billingInfoPage.fillData("NY");
			billingInfoPage.continueToNextStep();
			PaymentInformationPage paymentInfoPage = new PaymentInformationPage();
			assertTrue(isElementExisting(driver, PaymentInformationPage.invoiceMe_radiobutton.get(0), 5), "Navigated to payment information page after clicking on continue button from checkout page",test);
			paymentInfoPage.selectPaymentMethod(PaymentInformationPage.invoiceMe_radiobutton);
			paymentInfoPage.continueToReviewOrderPage();
			
			ReviewOrderPage reviewPage = new ReviewOrderPage();
			assertTrue(isElementExisting(driver, ReviewOrderPage.submitOrder_button, 5), "Navigated to confirmation page after clicking on continue button from payment method page",test);
			
			reviewPage.submitOrder();
			
			ReceiptPage receiptPage = new ReceiptPage();
			assertTrue(isElementExisting(driver, ReceiptPage.receipt_title, 15), "Navigated to receipt page after clicking on continue button from review order page",test);
		
			int expectedOrderId = Integer.parseInt(ReceiptPage.orderID_text.getText());
			
			OutlookMailPage outlookEmailPage = new OutlookMailPage();
			int actualOrderID = Integer.parseInt(outlookEmailPage.FetchorderIDFromOutlookEmail("mercer\\prathik-bhat", "Dec@1234", "CAD", test));
			
			verifyEquals(actualOrderID, expectedOrderId, test);
				
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify details for Canada_CU survey", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to Verify details for Canada_CU survey", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
}
