package ui.regression.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.assertFalse;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.DNNsitePurchase;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.HeaderMenuPage;
import verify.SoftAssertions;

public class TC2_CartPageVerification {

	@Test(enabled = true, priority = 1)
	public void navigateToSurveyAndReportsPage () throws Exception {
		try {
			
			test = reports.createTest("Navigate to survey and reports page");
			test.assignCategory("Regression");

			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveySite = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Navigation to survey and reports page failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Navigation to survey and reports page failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 2)
	public void verifyAddToCartFuntionForSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verifying if the survey is getting added to cart successfully");
			test.assignCategory("regression");
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			surveyPage.selectSurvey(DNNsiteSurveys.dnnSurvey_link);
				
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			
			surveyPage.selectSurveyQuantity("de76a5b4a3bd41ea845082de43c844c3");
			assertTrue((getElementText(DNNsiteSurveys.quantitySelected_message)).contains("You selected100copies atUSD 4.00each"), "Message for selected copies and its pricing is displayed.",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.totalPrice_text, 5), "Total cost for the surveys is displayed beside the Buy Now button",test);
			
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("100 copies @ 4.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 400.00"), "Price total label is displayed on the page",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Add to cart functionality failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Add to cart functionality failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 3)
	public void verifyEditCartFuntionForSurvey () throws Exception {
		try {
			
			test = reports.createTest("Verifying if user can edit the items added to the cart successfully");
			test.assignCategory("regression");
			
		
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.editItem_button, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			purchasePage.editItem();
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.selectQuantity_dropdown, 5), "Navigated to survey and reports page to edit the cart item",test);
			

			surveyPage.selectSurveyQuantity("d1ae30c72dc44c3d815dfcd46d011256");
			assertTrue((getElementText(DNNsiteSurveys.quantitySelected_message)).contains("You selected200copies atUSD 2.50each"), "Message for selected copies and its pricing is displayed.",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.totalPrice_text, 5), "Total cost for the surveys is displayed beside the Buy Now button",test);
			
			surveyPage.navigateToCartSummary();
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("200 copies @ 2.50 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 500.00"), "Price total label is displayed on the page",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Edit cart functionality failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Edit cart functionality failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 4)
	public void verifyEditCartFuntionViaCartFlyout () throws Exception {
		try {
			
			test = reports.createTest("Verifying if user can edit the items added to the cart successfully via cart flyout");
			test.assignCategory("regression");
			
			HeaderMenuPage headerPage = new HeaderMenuPage();
			headerPage.naviagteViaMenu(HeaderMenuPage.cartFlyout_menu,HeaderMenuPage.editCart_flyoutMenu);
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected surveyproduct buy page",test);
			
			surveyPage.selectSurveyQuantity("b55a701187404df58f030db4502a9566");
			assertTrue((getElementText(DNNsiteSurveys.quantitySelected_message)).contains("You selected500copies atUSD 1.30each"), "Message for selected copies and its pricing is displayed.",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.totalPrice_text, 5), "Total cost for the surveys is displayed beside the Buy Now button",test);
			
			surveyPage.navigateToCartSummary();
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("500 copies @ 1.30 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 650.00"), "Price total label is displayed on the page",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Edit cart functionality failed via cart flyout", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Edit cart functionality failed via cart flyout", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 5)
	public void removeItemFromCart () throws Exception {
		try {
			
			test = reports.createTest("Removing item from shopping cart");
			test.assignCategory("regression");
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			
			purchasePage.removeItem();
			
			HeaderMenuPage headerPage = new HeaderMenuPage();
			assertTrue(isElementExisting(driver, DNNsitePurchase.emptyCart_header, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertFalse(isElementExisting(driver, DNNsitePurchase.itemCountInCart, 5), "No items are present in the cart",test);
			

		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("remove item from cart functionality failed ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("remove item from cart functionality failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 6)
	public void verifyContinueShoppingLink() throws Exception {
		try {
			
			test = reports.createTest("Verification for continue shopping link from empty cart page");
			test.assignCategory("regression");
			
			DNNsitePurchase purchasePage = new DNNsitePurchase();
			assertTrue(isElementExisting(driver, DNNsitePurchase.emptyCart_header, 5), "Navigated to checkout page after clicking on Add To Cart button and item is removed from the cart",test);
			
			purchasePage.continueToShopping();
			
			DNNsiteSurveys surveyPage = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.dnnSurvey_link, 5), "Navigated to selected survey product select  page",test);
			
			surveyPage.selectSurvey(DNNsiteSurveys.dnnSurvey_link);
			
			assertTrue(isElementExisting(driver, DNNsiteSurveys.SurveyTitle, 5), "Navigated to selected survey product buy page",test);
			

			surveyPage.selectSurveyQuantity("de76a5b4a3bd41ea845082de43c844c3");
			assertTrue((getElementText(DNNsiteSurveys.quantitySelected_message)).contains("You selected100copies atUSD 4.00each"), "Message for selected copies and its pricing is displayed.",test);
			assertTrue(isElementExisting(driver, DNNsiteSurveys.totalPrice_text, 5), "Total cost for the surveys is displayed beside the Buy Now button",test);
			
			surveyPage.navigateToCartSummary();
			
			
			assertTrue(isElementExisting(driver, DNNsitePurchase.CartPageHeader, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertTrue(getElementText(DNNsitePurchase.priceSummary_text).equals("100 copies @ 4.00 each"), "Price summary text is displayed on the page",test);
			assertTrue(getElementText(DNNsitePurchase.totalAmount_label).equals("USD 400.00"), "Price total label is displayed on the page",test);
			purchasePage.removeItem();
			
			
			HeaderMenuPage headerPage = new HeaderMenuPage();
			assertTrue(isElementExisting(driver, DNNsitePurchase.emptyCart_header, 5), "Navigated to checkout page after clicking on Add To Cart button",test);
			assertFalse(isElementExisting(driver, DNNsitePurchase.itemCountInCart, 5), "No items are present in the cart",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification for continue shopping link from empty cart page failed ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Verification for continue shopping link from empty cart page failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

}
