package ui.regression.testcases.iMercer;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.HeaderMenuPage;
import pages.iMercer_pages.IndustriesPage;
import pages.iMercer_pages.LearningCenterPage;
import pages.iMercer_pages.Login;
import pages.iMercer_pages.MobilityPage;
import pages.iMercer_pages.RewardsPage;
import pages.iMercer_pages.ServicesPage;
import pages.iMercer_pages.WorkforcePage;
import verify.SoftAssertions;

public class TC7_MenuNavigationsVerificationUSRegion {
	
	@Test(enabled = true, priority = 1)
	public void changeTheRegionToUS () throws Exception {
		try {
			
			test = reports.createTest("Switch the region to US");
			test.assignCategory("regression");
	
			Login loginPage = new Login();
			loginPage.modifyRegion(Login.usRegion_link);
			verifyElementTextContains(Login.RegionBanner, "United States", test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to US FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Switching region to US FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 2)
	public void verifyNavigationViaHeaderMenu () throws Exception {
		try {
			
			test = reports.createTest("Navigating via all header menu");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.rewards_menu);
			RewardsPage rewardsPage = new RewardsPage();
			assertTrue(isElementExisting(driver, RewardsPage.rewardsPage_title, 5), "Navigated to REWARDS page successfully",test);
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.industries_menu);
			IndustriesPage industriesPage = new IndustriesPage();
			assertTrue(isElementExisting(driver, IndustriesPage.industriesPage_content, 5), "Navigated to INDUSTRIES page successfully",test);
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.mobility_menu);
			MobilityPage mobilityPage = new MobilityPage();
			assertTrue(isElementExisting(driver, MobilityPage.mobilityPage_title, 5), "Navigated to GLOBAL MOBILITY page successfully",test);
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.workforce_menu);
			WorkforcePage workforcePage = new WorkforcePage();
			assertTrue(isElementExisting(driver, WorkforcePage.workforcePage_title, 5), "Navigated to WORKFORCE page successfully",test);
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.learningCenter_menu);
			LearningCenterPage learningCenterPage = new LearningCenterPage();
			assertTrue(isElementExisting(driver, LearningCenterPage.learningCenterPage_title, 5), "Navigated to LEARNING CENTER page successfully",test);
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.services_menu);
			ServicesPage servicesPage = new ServicesPage();
			assertTrue(isElementExisting(driver, ServicesPage.servicesPage_title, 5), "Navigated to SERVICES page successfully",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	/*// Below testcase had page configuration issue thus commented for now. Can be run only after application is fixed
	@Test(enabled = true, priority = 3)
	public void verifyNavigationViaHeaderMenuToSubMenu () throws Exception {
		try {
			
			test = reports.createTest("Navigating via all header menu to sub menu");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.compensationRemuneration_link);
			RewardsPage rewardsPage = new RewardsPage();
			assertTrue(isElementExisting(driver, RewardsPage., 5), "Navigated to compensation remuneration page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.industries_menu, HeaderMenuPage.industrySolutionCategory_link);
			IndustriesPage industriesPage = new IndustriesPage();
			assertTrue(isElementExisting(driver, IndustriesPage., 5), "Navigated to Industry Solution category page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.mobility_menu, HeaderMenuPage.testing1_link);
			MobilityPage mobilityPage = new MobilityPage();
			assertTrue(isElementExisting(driver, MobilityPage., 5), "Navigated to Testing1 page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.workforce_menu, HeaderMenuPage.Communications_link);
			WorkforcePage workforcePage = new WorkforcePage();
			assertTrue(isElementExisting(driver, WorkforcePage., 5), "Navigated to Communications page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.learningCenter_menu, HeaderMenuPage.testCategory2_link);
			LearningCenterPage learningCenterPage = new LearningCenterPage();
			assertTrue(isElementExisting(driver, LearningCenterPage., 5), "Navigated to Test category 2 page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.services_menu, HeaderMenuPage.consulting_link);
			ServicesPage servicesPage = new ServicesPage();
			assertTrue(isElementExisting(driver, ServicesPage., 5), "Navigated to consulting page successfully",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to sub menu", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to sub menu", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 4)
	public void verifyNavigationViaHeaderMenuToBullet () throws Exception {
		try {
			
			test = reports.createTest("Navigating via all header menu to bullet");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.testingForNonVegUser_link);
			RewardsPage rewardsPage = new RewardsPage();
			assertTrue(isElementExisting(driver, RewardsPage.sportsVijaySurvey_title, 5), "Navigated to SPORTSSURVEYVIJAY page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.industries_menu, HeaderMenuPage.testing789_link);
			IndustriesPage industriesPage = new IndustriesPage();
			assertTrue(isElementExisting(driver, IndustriesPage., 5), "Navigated to TESTING789 page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.mobility_menu, HeaderMenuPage.testing5_link);
			MobilityPage mobilityPage = new MobilityPage();
			assertTrue(isElementExisting(driver, MobilityPage., 5), "Navigated to TESTING5 page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.workforce_menu, HeaderMenuPage.);
			WorkforcePage workforcePage = new WorkforcePage();
			assertTrue(isElementExisting(driver, WorkforcePage., 5), "Navigated to Health Care Communication Solutions page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.learningCenter_menu, HeaderMenuPage.);
			LearningCenterPage learningCenterPage = new LearningCenterPage();
			assertTrue(isElementExisting(driver, LearningCenterPage., 5), "Navigated to US Events/Trainings page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.services_menu, HeaderMenuPage.);
			ServicesPage servicesPage = new ServicesPage();
			assertTrue(isElementExisting(driver, ServicesPage., 5), "Navigated to Software Support Services page successfully",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to bullet", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to bullet", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}*/
	
	@Test(enabled = true, priority = 5)
	public void dnnPageMenuVerification () throws Exception {
		try {
			
			test = reports.createTest("Verification of main Menu links in US Region for DNN Pages");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menu, HeaderMenuPage.surveyAndReports_link);
			
			DNNsiteSurveys surveySite = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.rewards_menuUnitedStates);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Industries-and-consumer-goods1"), "Navigated to REWARDS page successfully",test);			
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.industries_menuUnitedStates);
			IndustriesPage industriesPage = new IndustriesPage();
			assertTrue(isElementExisting(driver, IndustriesPage.surveyTitle, 5), "Navigated to INDUSTRIES page successfully",test);
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.mobility_menuUnitedStates);
			MobilityPage mobilityPage = new MobilityPage();
			assertTrue(isElementExisting(driver, MobilityPage.surveyTitle, 5), "Navigated to Mobility page successfully",test);			
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.workforce_menuUnitedStates);
			WorkforcePage workforcePage = new WorkforcePage();
			assertTrue(isElementExisting(driver, WorkforcePage.surveyTitle, 5), "Navigated to workforce page successfully",test);			
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.learningCenter_menuUnitedStates);
			LearningCenterPage learningPage = new LearningCenterPage();
			assertTrue(isElementExisting(driver, LearningCenterPage.surveyTitle, 5), "Navigated to Learning Center page successfully",test);			
			
			headerMenuPage.hederMenuClick(HeaderMenuPage.services_menuUnitedStates);
			ServicesPage servicesPage = new ServicesPage();
			assertTrue(isElementExisting(driver, ServicesPage.surveyTitle, 5), "Navigated to Services page successfully",test);			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed Verification of main Menu links in US Region for DNN Pages", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed Verification of main Menu links in US Region for DNN Pages", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 6)
	public void verifyMainMenuInDNN () throws Exception {
		try {
			
			test = reports.createTest("Navigating via all header menu to sub menu DNN");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.compensationRemuneration_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Rewards/Compensation-And-Remuneration"), "Navigated to compensation remuneration page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.industries_menuUnitedStates, HeaderMenuPage.consumerGood_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/iMercerDNN741/products/Testing_Apr-3_Wizard-COPY-COPY-COPY?isPartOpen=true"), "Navigated to Consumer good  page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.mobility_menuUnitedStates, HeaderMenuPage.coreSolutions_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Global-Mobility/Core-Solutions"), "Navigated to core solution page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.workforce_menuUnitedStates, HeaderMenuPage.Communications_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Workforce-Solutions/Communications"), "Navigated to communication page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.learningCenter_menuUnitedStates, HeaderMenuPage.usEvent_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Learning-Center/US-Events-Trainings-On-Demand"), "Navigated to US events page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.services_menuUnitedStates, HeaderMenuPage.services_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Services/Services"), "Navigated to services page successfully",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to sub menu DNN", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to sub menu DNN", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 7)
	public void verifySubMenuInDNN () throws Exception {
		try {
			
			test = reports.createTest("Navigating via all header menu to bullets DNN");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.surveyAndReports_link);
			DNNsiteSurveys survey = new DNNsiteSurveys();
			assertTrue(isElementExisting(driver, DNNsiteSurveys.surveyAndReports_headerTitle, 5), "Navigated to survey and reports page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.mobility_menuUnitedStates, HeaderMenuPage.qualityOfLiving_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Global-Mobility/Core-Solutions/Quality-of-Living"), "Navigated to quality of life page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.workforce_menuUnitedStates, HeaderMenuPage.healthCareComminication_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Workforce-Solutions/Communications/Health-Care-Communication-Solutions"), "Navigated to health care communication page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.learningCenter_menuUnitedStates, HeaderMenuPage.IPElearning_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Learning-Center/US-Events-Trainings-On-Demand/IPE-eLearning"), "Navigated to IPE eLearning page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.services_menuUnitedStates , HeaderMenuPage.consulting_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Services/Services/Consulting"), "Navigated to consulting page successfully",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to bullets DNN", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via header menu to bullets DNN", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 8)
	public void verifySubMenuUnderBullets () throws Exception {
		try {
			
			test = reports.createTest("Navigating via menu under bullets DNN");
			test.assignCategory("regression");
	
			HeaderMenuPage headerMenuPage = new HeaderMenuPage();
		
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.rewards_menuUnitedStates, HeaderMenuPage.bullet123_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Rewards/Compensation-And-Remuneration/Surveys-Search-by-Job/123"), "Navigated to page successfully",test);
			
			headerMenuPage.naviagteViaMenu(HeaderMenuPage.workforce_menuUnitedStates, HeaderMenuPage.globalDiversityForum_link);
			assertTrue(driver.getCurrentUrl().trim().equals("https://qa12-www2.imercer.com/imercerdnn741/Workforce-Solutions/Diversity-Inclusion/D-I-Peer-Executive-Networks/Global-Diversity-Forum"), "Navigated to page successfully",test);
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via menu under bullets DNN", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to navigate via menu under bullets DNN", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
}
