package utilities;


import java.io.InputStream;
import java.util.Properties;

import org.testng.asserts.SoftAssert;



/**
 * @author YugandharReddy
 */
public class InitTests {
	public static SoftAssert softAssert=new SoftAssert();

	public static Properties props = new Properties();
	/**
	 * @description:Initialization OS,version,browser and url details.
	 * 
	 * @throws Exception
	 */
	public static String OS_VERSION = "";
	public static String ENDPOINT_LOGIN = "";
	public static String CaptureScreenshotOnFail = "";
	public static String OS_NAME = "";
	public static String BROWSER_TYPE = "";
	public static String BASEURL = "";

	public static String USERNAME = "";
	public static String PASSWORD = "";

	public static String dir_path;
	public static String REST_URL_PURCHASE_JOBS = "";

	public static String PROJ_CONFIG_Path=""; 

	public static final String TestCases = "";
	public static String BROWSER_VERSION = "";
	public static String PLATFORM = "";
	public static String SAUCE_USERNAME = "";
	public static String SAUCE_ACCESSKEY = "";
	public static String SAUCE_URL= "";
	public static String PARENT_TUNNEL = "";
	public static String TUNNEL_IDETIFIER = "";
	public static String RESOLUTION = "";
	public static String EXECUTION_ENV = "";
	public static String node_URL;

	public static  InputStream input;
	
	public static int waitTimeout ;


	public InitTests() {
		try {
			System.out.println("Getting config files..");

			System.out.println("Config read successfull");
			ClassLoader loader = this.getClass().getClassLoader();
			 InputStream input = loader.getResourceAsStream("config/testdata.properties");
			props.load(input);
			dir_path = props.getProperty("userdir");
			System.out.println("dir path"+dir_path);
			PROJ_CONFIG_Path="//resources//config//Project_Config.properties";
		
			REST_URL_PURCHASE_JOBS=props.getProperty("removePurchaseJobsRestUrl");

			waitTimeout=Integer.parseInt(props.getProperty("explicitWaitInSec"));
			BASEURL = props.getProperty("baseurl");

			USERNAME = props.getProperty("username");
			PASSWORD = props.getProperty("password");
			BROWSER_TYPE = props.getProperty("browser");
			OS_VERSION = props.getProperty("os_version");
			OS_NAME = props.getProperty("os_name");
			CaptureScreenshotOnFail=props.getProperty("CaptureScreenshotOnFail");
			SAUCE_URL=props.getProperty("sauce_url");
			PARENT_TUNNEL = props.getProperty("parent_tunnel");
			TUNNEL_IDETIFIER = props.getProperty("tunnel_identifier");
			PLATFORM = props.getProperty("platform");
			RESOLUTION=props.getProperty("resolution");
			BROWSER_VERSION = props.getProperty("browser_version");
			EXECUTION_ENV = props.getProperty("execution_env");
			node_URL=props.getProperty("node_url");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		catch (Error ex) {
			ex.printStackTrace();
		}
	}
	
	public InitTests(String appName) {
		try {
			System.out.println("Getting config files..");

			System.out.println("Config read successfull");
			ClassLoader loader = this.getClass().getClassLoader();
			input = loader.getResourceAsStream("config/testdata.properties");
			props.load(input);
			dir_path = props.getProperty("userdir");
			System.out.println("dir path"+dir_path);
			PROJ_CONFIG_Path="//resources//config//Project_Config.properties";
		
			REST_URL_PURCHASE_JOBS=props.getProperty("removePurchaseJobsRestUrl");

			waitTimeout=Integer.parseInt(props.getProperty("explicitWaitInSec"));
			BASEURL = props.getProperty(appName+"_baseurl");

			USERNAME = props.getProperty(appName+"_username");
			PASSWORD = props.getProperty(appName+"_password");
			BROWSER_TYPE = props.getProperty("browser");
			OS_VERSION = props.getProperty("os_version");
			OS_NAME = props.getProperty("os_name");
			CaptureScreenshotOnFail=props.getProperty("CaptureScreenshotOnFail");
			SAUCE_URL=props.getProperty("sauce_url");
			PARENT_TUNNEL = props.getProperty("parent_tunnel");
			TUNNEL_IDETIFIER = props.getProperty("tunnel_identifier");
			PLATFORM = props.getProperty("platform");
			RESOLUTION=props.getProperty("resolution");
			BROWSER_VERSION = props.getProperty("browser_version");
			EXECUTION_ENV = props.getProperty("execution_env");
			node_URL=props.getProperty("node_url");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		catch (Error ex) {
			ex.printStackTrace();
		}
	}

	public static String getPropValue(String key) {
		return props.getProperty(key);
	}
}
