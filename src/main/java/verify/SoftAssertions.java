package verify;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import static utilities.InitTests.dir_path;
import static utilities.InitTests.softAssert;
import static driverfactory.Driver.isElementExisting;

/**
 * @author YugandharReddyGorrep
 *
 */
public class SoftAssertions {
	// public static SoftAssert softAssert;ic static SoftAssert softAssert;
	public static void verifyEquals(String actual, String expected, ExtentTest test) {

		if (actual.equals(expected)) {
			test.log(Status.PASS, "verifyEquals() " + "actual " + actual + " expected " + expected);
			ATUReports.add("verifyEquals() ", "Locator--", expected, actual, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.valueOf(actual)));
		} else {
			test.log(Status.FAIL, "verifyEquals() " + "actual " + actual + " but " + " expected " + expected);
			ATUReports.add("verifyEquals() ", "Locator--", expected, actual, LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
	}

	public static void verifyTableContent(WebElement ele, String tableName, String strValue, ExtentTest test) {
		boolean result = false;
		int j = 0;
		String subValues[] = strValue.split("~");
		for (int i = 0; i < subValues.length; i++) {
			subValues[i] = subValues[i].replace(" ", "");
			subValues[i] = subValues[i].replace("\\n", "");
			subValues[i] = subValues[i].replace("\n", "");
			subValues[i] = subValues[i].replace("\\", "");
			subValues[i] = subValues[i].replace("&nbsp;", "");
			subValues[i] = subValues[i].replace("&", "");
		}
		String strInnerHTML = ele.getAttribute("outerHTML");
		if (strInnerHTML != null) {
			strInnerHTML = strInnerHTML.replace(" ", "");
			strInnerHTML = strInnerHTML.replace("\\n", "");
			strInnerHTML = strInnerHTML.replace("\n", "");
			strInnerHTML = strInnerHTML.replace("\\", "");
			strInnerHTML = strInnerHTML.replace("&nbsp;", "");
			strInnerHTML = strInnerHTML.replace("&amp;", "");
			strInnerHTML = strInnerHTML.toLowerCase();
			strInnerHTML = strInnerHTML.replace("<br>", "");
			strInnerHTML = strInnerHTML.replace("<b>", "");
			strInnerHTML = strInnerHTML.replace("</b>", "");
		} else
			result = false;
		for (int i = 0; i < subValues.length; i++) {
			if (strInnerHTML.contains(subValues[i].toLowerCase()))
				j = j + 1;
		}

		if (j == subValues.length)
			result = true;
		else
			result = false;

		if (result) {
			test.log(Status.PASS, tableName);
			ATUReports.add(tableName, strValue, tableName + "contains" + strValue, tableName + "contains" + strValue,
					LogAs.PASSED, new CaptureScreen(ele));
		} else {
			test.log(Status.FAIL, tableName);
			ATUReports.add("verifyElementTextIgnoreCase() ", tableName + "contains" + strValue,
					tableName + "not contains" + strValue, LogAs.FAILED, new CaptureScreen(ele));
		}
	}

	public static void verifyElementExits(WebElement e, String expected, ExtentTest test) {
		boolean flag = isElementExisting(e, 20);
		if (flag) {
			test.log(Status.PASS, "verifyElementExits() ");
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected,
					"page containts" + expected, LogAs.PASSED, new CaptureScreen(e));
		} else {
			test.log(Status.FAIL, "verifyElementContains() ");
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected,
					"page containts" + expected, LogAs.PASSED, new CaptureScreen(e));
		}
	}


	public static void verifyNotEquals(String actual, String expected, ExtentTest test) {

		if (!actual.equals(expected)) {
			test.log(Status.PASS, "verifyNotEquals()--" + "actual " + actual + " expected " + expected);
			ATUReports.add("verifyNotEquals() ", "Locator--", expected, actual, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.valueOf(actual)));
		} else {
			test.log(Status.FAIL, "verifyNotEquals()--" + "actual " + actual + "but" + " expected " + expected);
			ATUReports.add("verifyNotEquals() ", "Locator--", expected, actual, LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.valueOf(actual)));

		}
	}

	public static void verifyEquals(int act, int exp, ExtentTest test) {
		// TODO Auto-generated method stub
		if (act == exp) {
			test.log(Status.PASS, "verifyEquals() " + "actual " + act + " expected " + exp);
			ATUReports.add("verifyEquals() ", "Locator--", new Integer(exp).toString(), new Integer(act).toString(),
					LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			test.log(Status.FAIL, "verifyEquals() " + "actual " + act + " but " + " expected " + exp);
			ATUReports.add("verifyEquals() ", "Locator--", new Integer(exp).toString(), new Integer(act).toString(),
					LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
	}

	public static void verifyContains(String act, String exp, String message, ExtentTest test) {
		// TODO Auto-generated method stub
		if (act.contains(exp)) {
			test.log(Status.PASS, "verifyEquals() " + "actual " + act + " expected " + exp);
			ATUReports.add("verifyEquals() ", "Locator--", exp, act, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
		} else {
			test.log(Status.FAIL, "verifyEquals() " + "actual " + act + " but " + " expected " + exp);
			ATUReports.add("verifyEquals() ", "Locator--", exp, act, LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
		}
	}

	public static void verifyElementTextContains(WebElement e, String expected, ExtentTest test) {
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value").trim();
		else
			actual = e.getText().trim();
		if (actual.toUpperCase().contains(expected.trim().toUpperCase())) {
			test.log(Status.PASS,
					"verifyElementContains()" + e.getAttribute("outerHTML") + " contains text as " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else {
			test.log(Status.FAIL, "verifyElementContains() " + e.getAttribute("outerHTML") + " has actual value "
					+ actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}

	public static void verifyElementText(WebElement e, String expected, ExtentTest test) {
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.equals(expected)) {
			test.log(Status.PASS, "verifyElementText() " + "Webelement " + e.getAttribute("outerHTML")
					+ " contains text as " + expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else {
			test.log(Status.FAIL, "verifyElementText() " + e.getAttribute("outerHTML") + " has actual " + actual
					+ " but" + " expected " + expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}

	public static void verifyElementTextIgnoreCase(WebElement e, String expected, ExtentTest test) {
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.toUpperCase().equals(expected.toUpperCase())) {
			test.log(Status.PASS, "verifyElementTextIgnoreCase() " + "Webelement " + e.getAttribute("outerHTML")
					+ " contains text as " + expected);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else {
			test.log(Status.FAIL, "verifyElementTextIgnoreCase() " + e.getAttribute("outerHTML") + " has actual "
					+ actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}

	public static void verifyElementHyperLink(WebElement e, ExtentTest test) {
		if (e.getTagName().contains("a")) {
			test.log(Status.PASS,
					"verifyElementHyperLink()" + "Webelement " + e.getAttribute("outerHTML") + " contains hyperlink");
		} else {
			test.log(Status.FAIL, "verifyElementHyperLink() " + "Webelement " + e.getAttribute("outerHTML")
					+ " have actual " + e.getTagName() + " but " + " expected " + "a");
		}
	}

	public static void verifyElementContains(WebElement e, String expected, String message, ExtentTest test) {
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.toUpperCase().contains(expected.toUpperCase())) {
			test.log(Status.PASS, "verifyElementContains() " + "Webelement " + e.getAttribute("outerHTML")
					+ " contains text as " + expected + message);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else {
			test.log(Status.FAIL, "verifyElementContains() " + e.getAttribute("outerHTML") + "has actual " + actual
					+ " but" + " expected " + expected);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}

	public static void verifyElementText(WebElement e, String expected, String message, ExtentTest test) {
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.equals(expected)) {
			test.log(Status.PASS, "verifyElementText() " + "Webelement " + e.getAttribute("outerHTML")
					+ " contains text as " + expected + message);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else {
			test.log(Status.FAIL, "verifyElementText() " + "actual " + actual + "but" + " expected " + expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}

	public static void verifyElementTextIgnoreCase(WebElement e, String expected, String message, ExtentTest test) {
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.toUpperCase().equals(expected.toUpperCase())) {
			test.log(Status.PASS, "verifyElementTextIgnoreCase() " + "Webelement " + e.getAttribute("outerHTML")
					+ " contains text as " + expected + message);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else {
			test.log(Status.FAIL,
					"verifyElementTextIgnoreCase() " + "actual " + actual + "but" + " expected " + expected);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}

	public static void verifyElementLink(WebElement e, String message, ExtentTest test) {
		if (e.getTagName().contains("a")) {
			test.log(Status.PASS, "verifyElementLink()" + "Webelement " + e.getAttribute("outerHTML")
					+ " contains hyperlink" + message);
		} else {
			test.log(Status.FAIL, "verifyElementLink() " + "actual " + e.getTagName() + "but" + " expected " + "a");
		}
	}

	public static void assertTrue(boolean condition, String message, ExtentTest test) {
		if (condition == true) {
			test.log(Status.PASS, "assertTrue()" + message);
			ATUReports.add("assertTrue() ", message, "" + condition + "", LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
			
			System.out.println("Passed finally ------------------------------------------------------");
		} else {
			test.log(Status.FAIL, "assertTrue()" + condition);
			ATUReports.add("assertTrue() ", message, "" + condition + "", LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
		}
	}

	public static void assertFalse(boolean condition, String message, ExtentTest test) {
		if (condition == false) {
			test.log(Status.PASS, "assertFalse()" + message);
		} else {
			test.log(Status.FAIL, "assertFalse()");
		}
	}

	public static void assertNull(Object obj, String message, ExtentTest test) {
		if (obj == null) {
			test.log(Status.PASS, "assertNull()" + message);
		} else {
			test.log(Status.FAIL, "assertNull()");
		}
	}

	public static void assertNotNull(Object obj, String message, ExtentTest test) {
		if (obj != null) {
			test.log(Status.PASS, "assertNotNull()" + obj.toString() + message);
		} else {
			test.log(Status.FAIL, "assertNotNull()");
		}
	}

	public static void fail(Throwable e, String pathFrScreenshot, ExtentTest test) throws IOException {

		String excFile = "/src/main/WebContent/extentReports/exception.txt";
		createExceptionFile(excFile, e);
		String content = new String(Files.readAllBytes(Paths.get(dir_path + excFile)));
		test.log(Status.FAIL, "fail()-Exception" + content);
		if (InitTests.CaptureScreenshotOnFail.equalsIgnoreCase("true")) {
			test.log(Status.INFO, "fail()-Exception" + test.addScreenCaptureFromPath(pathFrScreenshot));
		}
		softAssert.fail();

	}

	private static void createExceptionFile(String path, Throwable e) throws IOException {
		File f = new File(dir_path + path);
		if (!f.exists())
			f.createNewFile();
		PrintWriter p = new PrintWriter(dir_path + path);
		e.printStackTrace(p);
		p.close();
	}

	public static void verifyElementListTextIgnoreCase(List<WebElement> elementList, String[] expected,
			ExtentTest test) {
		int i = 0;
		for (WebElement e : elementList) {

			String actual;
			if (e.getText().isEmpty())
				actual = e.getAttribute("value");
			else
				actual = e.getText();
			if (actual.toUpperCase().trim().equals(expected[i].toUpperCase())) {
				test.log(Status.PASS, "verifyElementTextIgnoreCase() " + "Webelement " + e.getAttribute("outerHTML")
						+ " contains text as " + expected[i]);
				ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected[i], actual,
						LogAs.PASSED, new CaptureScreen(e));
			} else {
				test.log(Status.FAIL, "verifyElementTextIgnoreCase() " + e.getAttribute("outerHTML") + " has actual "
						+ actual + " but" + " expected " + expected[i]);
				ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected[i], actual,
						LogAs.FAILED, new CaptureScreen(e));
			}
			i++;
		}
	}
}
